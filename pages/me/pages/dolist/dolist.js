import util from '../../../../utils/util';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    participantInfo:{},
    myDoPlusList:[],
    doPlusContent: [], 
    discoveryPageInfo: {
      pageNum: 1,
      pageSize: 10
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.init();
  },
  init:function(){
    var that = this;
    var discoveryPageInfo = that.data.discoveryPageInfo;
    util.wxRequest({
      url:'getMyDoPlusListPageInfo',
      data: { discoveryPageInfo: discoveryPageInfo},
      success:function(res){
        var myDoPlusList = res.myDoPlusList;
        var discoveryPageInfo = res.discoveryPageInfo;
        discoveryPageInfo.pageNum++;
        for (var i = 0; i < myDoPlusList.length;i++){
          var item = myDoPlusList[i];
          var imageContent = item.doPlusContent.imageContent;
          if (imageContent && imageContent.length > 4){
            item.doPlusContent.imageContent = imageContent.slice(0, 4);
          }
          if (imageContent && imageContent.length){
            item.imageContentLen = imageContent.length;
          }else{
            item.imageContentLen = 0;
          }
          var createTime = item.createTime;
          if (createTime) {
            item.month = +(createTime.substring(5, 7));
            item.day = createTime.substring(8, 10);
          }
          if (item.doPlusContent.soundContent) {
            var thumb = item.doPlusContent.soundContent.soundSeconds/1000;
            var formatduration = util.formarAudioTime(thumb);
            item.nowduration = '0"';
            item.context = item.doPlusContent.soundContent.soundUrl;
            item.formatduration = formatduration;
          }
        }
        var allmyDoPlusList = myDoPlusList;
        if (discoveryPageInfo.pageNum > 2){
          allmyDoPlusList = that.data.myDoPlusList.concat(myDoPlusList);
        }
        that.setData({
          discoveryPageInfo:discoveryPageInfo,
          myDoPlusList: allmyDoPlusList
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },
  
  gododetail:function(e){
    var doPlusId = e.currentTarget.dataset.doplusid;
    var url = '../../../home/pages/detail/detail?doPlusId=' + doPlusId;
    wx.navigateTo({
      url: url
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var discoveryPageInfo =  that.data.discoveryPageInfo;
    if (discoveryPageInfo && discoveryPageInfo.size >= discoveryPageInfo.pageNum){
      that.init();
    }
  }
})