import util from '../../../../utils/util';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageInfo: {
      pageNum: 1,
      pageSize: 1000
    },
    readPicBookDisplayItemList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.init();
  },
  init: function () {
    var that = this; 
    var pageInfo = that.data.pageInfo;
    util.wxRequest({
      url: 'getPlayPicBookPageInfo',
      data: pageInfo,
      success: function (res) {
        that.setData({
          readPicBookDisplayItemList: res.readPicBookDisplayItemList || []
        })
      }
    })
  },
  quitChannel: function (e) {
    var that = this;
    var channelId = e.currentTarget.dataset.channelid;
    wx.showModal({
      title: '',
      content: '确定要退出频道吗',
      success: function (res) {
        if (res.confirm) {

          util.wxRequest({
            url: 'quitChannel',
            data: { channelId: channelId },
            success: function (res) {
              that.init();
            }
          });
        } else if (res.cancel) {

        }
      }
    })


  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  }
})