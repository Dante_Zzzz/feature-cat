import util from '../../../../utils/util';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    wechatGroupInfoList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.init();
  },
  init:function(){
    var that = this;
    util.wxRequest({
      url:'getWechatGroupInfo',
      success:function(res){
        that.setData({
          wechatGroupInfoList: res.wechatGroupInfoList
        })
      }
    })
  },
  deleteWechatGroupRelationship:function(e){
    var that = this;
    var gid = e.currentTarget.dataset.gid;
    util.wxRequest({
      url: 'deleteWechatGroupRelationship',
      data:{gid:gid},
      success: function (res) {
        that.init()
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  }
})