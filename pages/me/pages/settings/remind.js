import util from '../../../../utils/util';

Page({
  /**
   * 页面的初始数据
   */
  data: {
    channelNotifyList:[]
  },
  switchChange:function(e){
    var index = e.currentTarget.dataset.index;
    var value = e.detail.value;
    console.log(e);

    var channelNotifyList = this.data.channelNotifyList;
    channelNotifyList[index].notifySwitch = value;
    this.setData({
      channelNotifyList: channelNotifyList
    })
    
  },
  bindTimeChange: function (e) {
    var index = e.currentTarget.dataset.index;
    var channelNotifyList = this.data.channelNotifyList;
    var value = e.detail.value;
    channelNotifyList[index].notifyTime = value;
    this.setData({
      channelNotifyList: channelNotifyList
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.init()
  },
  init:function(){
    var that = this;
    util.wxRequest({
      url:'getNotifyPageInfo',
      data:{},
      success:function(res){
        var channelNotifyList = res.channelNotifyList;
        if (channelNotifyList){
          for (var i = 0; i < channelNotifyList.length; i++) {
            var notifyTime = channelNotifyList[i].notifyTime;
            channelNotifyList[i].notifyTime = notifyTime.replace(/-/g, ':');
          }
          that.setData({
            channelNotifyList: channelNotifyList
          })
        }
      
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})