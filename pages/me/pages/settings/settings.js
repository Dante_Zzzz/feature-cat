// pages/me/pages/settings/settings.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userRole: wx.getStorageSync('userRole')
  },
  goabout:function(){
    wx.navigateTo({
      url: 'about'
    })
  },
  goremind:function(){
    wx.navigateTo({
      url: 'remind'
    })
  },
  gochannellist(){
    wx.navigateTo({
      url: 'channellist'
    })
  },
  gogrouprelation(){
    wx.navigateTo({
      url: 'grouprelation'
    })
  },
  gosettings:function(){
    wx.navigateTo({
      url: '/pages/operation/pages/index/index'
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  }
})