import util from '../../../../utils/util'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    tab:0,
    messageList:[],
    sysMessageList: [],
    pageInfo:{
      pageNum:1,
      pageSize:10
    },
    sysPageInfo: {
      pageNum: 1,
      pageSize: 10
    }
  },
  changetab:function(e){
    var tab = e.currentTarget.dataset.tab;
    this.setData({
      tab: tab
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      pageInfo: {
        pageNum: 1,
        pageSize: 10
      },
      sysPageInfo: {
        pageNum: 1,
        pageSize: 10
      }
    })
    this.init();
    this.initSysMessage();
  },
  refreshPrevPage: function () {
    var pages = getCurrentPages();
    if (pages.length > 1) {
      var prePage = pages[pages.length - 2];
      prePage.init();
    }
  },
  initSysMessage:function(){
    var that = this;
    var messageType = 3;
    var sysPageInfo = that.data.sysPageInfo;
    util.wxRequest({
      url: 'getMessageListPageInfo',
      data: { messageType: messageType, pageInfo: sysPageInfo },
      success: function (res) {
        var messageList = res.messageList;
        if (messageList && messageList.length > 0){
          var sysPageInfo = res.pageInfo;
          sysPageInfo.pageNum++;

          for (var i = 0; i < messageList.length; i++) {
            var item = messageList[i];
            var messageJosnStr = item.messageJosnStr;
            try {
              item.messageJosnStr = JSON.parse(messageJosnStr);
            } catch (e) { }
          }
          var allmessageList = messageList;
          if (sysPageInfo.pageNum > 2) {
            allmessageList = that.data.sysMessageList.concat(messageList);
          }
          that.setData({
            sysPageInfo: sysPageInfo,
            sysMessageList: allmessageList
          })
        }
        
      }
    })
  },
  init:function(){
    var that = this;
    var messageType = 1;
    var pageInfo = that.data.pageInfo;
    util.wxRequest({
      url:'getMessageListPageInfo',
      data: { messageType: messageType, pageInfo: pageInfo},
      success:function(res){
        var messageList = res.messageList;
        if (messageList && messageList.length >0){
          var pageInfo = res.pageInfo;
          pageInfo.pageNum++;
          for (var i = 0; i < messageList.length;i++){
            var item = messageList[i];
            var messageJosnStr = item.messageJosnStr;
            try{
              item.messageJosnStr = JSON.parse(messageJosnStr); 
            }catch(e){}
          }
          var allmessageList = messageList;
          if (pageInfo.pageNum > 2) {
            allmessageList = that.data.messageList.concat(messageList);
          }
          that.setData({
            pageInfo:pageInfo,
            messageList: allmessageList
          })
          that.refreshPrevPage();

        }

      }
    })
  },
  gododetail: function (e) {
    var doPlusId = e.currentTarget.dataset.doplusid;
    var url = '../../../home/pages/detail/detail?doPlusId=' + doPlusId;
    wx.navigateTo({
      url: url
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var tab = that.data.tab;
    if(tab == 0){
      var pageInfo = that.data.pageInfo;
      if (pageInfo && pageInfo.size >= pageInfo.pageNum) {
        that.init();
      }
    }else{
      var sysPageInfo = that.data.sysPageInfo;
      if (sysPageInfo && sysPageInfo.size >= sysPageInfo.pageNum) {
        that.initSysMessage();
      }
    }
    
  }
  
})