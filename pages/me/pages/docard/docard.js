import util from '../../../../utils/util'
const Promise = require('../../../common/libs/es6-promise')
var _ = require('../../../common/libs/lodash.core.min.js');
var app = getApp();
var days = ["一", "二", "三", "四", "五", "六", "日"];
Page({

  /**
   * 页面的初始数据
   */
  data: {
    show:false,
    isLoading:false,
    currentNavtab:0,
    pUrlsPath:[],
    canNext:false,
    canPrev:false,
    imgUrls:[],
    year:'',
    weeK:'',
    nowYear:'',
    nowWeek:'',
    dayOfWeekToday:'',
    cardlist:[],
    weeklyDoCardMap:[],
    showShareImg:false,
    indicatorDots: false,
    autoplay: false,
    interval: 5000,
    duration: 1000
  },
  longTapHandle:function(e){
    this.save(e);
  },
  getYearAndWeek:function(flag){
    var data = this.data;
    var week = data.week;
    var year = data.year;
    if (flag == 'next') {
      week++;
      if (week >= 53) {
        week = 1;
        year++;
      }
    }else{
      week--;
      if (week == 0) {
        week = 52;
        year--;
      }
    }
    this.setData({
      year:year,
      week:week
    });
    return {yearAndWeek:year + '-' + week};
  },
  prevHandle:function(e){
    if (this.data.isLoading){
      return;
    }
    if(this.data.canPrev){
      this.init(this.getYearAndWeek('prev'));
    }
  },
  nextHandle: function (e) {
    if (this.data.isLoading) {
      return;
    }
    if(this.data.canNext){
      this.init(this.getYearAndWeek('next'));
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showShareMenu({
      withShareTicket: true
    });
    this.init();
  },
  init:function(params){
    var that = this;
    var param = params || {};
    var data = that.data;
    that.setData({
      isLoading:true
    })
    util.wxRequest({
      url: "getDoCardListPageInfo",
      data: param,
      success:function(res){
        that.setData({
          isLoading: false
        })
        var yearAndWeek = res.yearAndWeek;
        var weeklyDoCardMap = res.weeklyDoCardMap;
        var dayOfWeekToday = res.dayOfWeekToday;
        if(yearAndWeek){
          var cardlist = [];
          var imgUrls = [];
          var cardImgUrl = '';
          var cardDate = '';
          for (var i in weeklyDoCardMap) {
            var items = weeklyDoCardMap[i];
            var cardDayOfWeek = items.cardDayOfWeek;
            items.day = days[cardDayOfWeek-1];
            var dailyDoCardList = items.dailyDoCardList;
            if (cardDayOfWeek == dayOfWeekToday) {
              for (var j in dailyDoCardList) {
                var v = dailyDoCardList[j];
                cardImgUrl = items.cardImgUrl;
                cardDate = items.cardDate;
                imgUrls.push({
                  channelId: v.channelId,
                  cardImgUrl: cardImgUrl,
                  cardDate: cardDate,
                  accumulateChannelDoCard: v.accumulateChannelDoCard,
                  channelName: v.channelName,
                  participantImgUrl: v.participantImgUrl,
                  participantName: v.participantName,
                  miniProgramQRCodeImgUrl: v.miniProgramQRCodeImgUrl
                });
              }
            }
            cardlist.push(items);
          }
          var tmp = yearAndWeek.split('-');
          var year = tmp[0];
          var week = tmp[1];
          var nowYear = data.nowYear;
          var nowWeek = data.nowWeek;
          if(_.isEmpty(param)){
            nowYear = year;
            nowWeek = week;
            that.setData({
              nowYear: nowYear,
              nowWeek: nowWeek
            })
          }
          var canNext = true;
          var canPrev=true;
          if (year == nowYear && week == nowWeek) {
            canNext = false;
          }
          if (year == 2018 && week == 1){
            canPrev = false;
          }
          that.setData({
            canNext:canNext,
            canPrev: canPrev,
            year:year,
            week:week,
            dayOfWeekToday:dayOfWeekToday,
            cardlist: cardlist,
            weeklyDoCardMap: weeklyDoCardMap,
            imgUrls:imgUrls,
            show: true
          })
        }
      },
      onfail:function(){
        that.setData({
          isLoading: false
        })
      }
    })
  },
  save: function (e) {
    wx.showLoading();
    var that = this;
    var currentNavtab = that.data.currentNavtab || 0;
    var imgUrls = that.data.imgUrls;
    var imgUrl = imgUrls[currentNavtab];
    var channelId = imgUrl.channelId;
    var pageUrl = 'pages/discover/pages/channelinfo/channelinfo';
    util.wxRequest({
      url:'getQrcodeImg',
      data: { 
        pageUrl: pageUrl, 
        scene:channelId
      },
      success:function(res){
        var miniProgramQRCodeImgUrl = res.imgStr;
        var promiseList = [];
        var participantImgUrlpromise = that.getImageInfo(imgUrl.participantImgUrl);
        var cardpromise = that.getImageInfo(imgUrl.cardImgUrl);
        var miniProgramQRCodeImgUrlpromise = that.getImageInfo(miniProgramQRCodeImgUrl);
        promiseList.push(cardpromise);
        promiseList.push(participantImgUrlpromise);
        promiseList.push(miniProgramQRCodeImgUrlpromise);
        Promise.all(promiseList).then(function (data) {
          imgUrl.cardImgUrl = data[0].temp || '../../../../images/do_card_default';
          imgUrl.avatarUrl = data[1].temp || '../../../../images/default.png';
          imgUrl.xcxUrl = data[2].temp;
          that.drawImg("shareCanvas", imgUrl);
        });

      }, 
      onfail: function () {

      },
      isHideLoading: true
    })
    


  },

  share: function () {
    wx.showShareMenu({
      withShareTicket: true
    })
  },
  getImageInfo: function (src) {
    const deferred = util.getDeferred()
    var that = this;
    wx.downloadFile({
      url: src,
      success: function (data) {
        var tempFilePath = data.tempFilePath;
        deferred.resolve({ src: src, temp: tempFilePath });
      },
      fail: function (data) {
        console.log(data)
        deferred.resolve({ src: src, temp: '' });
      }
    })
    
    return deferred.promise;
  },
  
  swiperChange:function(e){
    this.setData({
      currentNavtab: e.detail.current
    })  
  },
  getTempPath: function (imgUrls, cardImgUrl, cardDate){
    var that = this;
    var cardpromise = that.getImageInfo(cardImgUrl);
    var promiseList = [];
    promiseList.push(cardpromise);
    for (var i in imgUrls){
      var item = imgUrls[i];
      var participantImgUrl = item.participantImgUrl || '../../../../images/default.png';
      var miniProgramQRCodeImgUrl = item.miniProgramQRCodeImgUrl || '../../../../images/default.png';
      var participantImgUrlpromise = that.getImageInfo(participantImgUrl);

      var miniProgramQRCodeImgUrlpromise = that.getImageInfo(miniProgramQRCodeImgUrl);
      promiseList.push(participantImgUrlpromise);
      promiseList.push(miniProgramQRCodeImgUrlpromise);
    }
    Promise.all(promiseList).then(function (data) {
      var cardImgUrl = data[0].temp;
      for (var j = 0; j < imgUrls.length; j++) {
        var img = imgUrls[j];
        for (var i = 0; i < data.length; i++) {
          var value = data[i];
          if (img.participantImgUrl == value.src){
            img.avatarUrl = value.temp;
          }
          if (img.miniProgramQRCodeImgUrl == value.src || '../../../../images/default.png' == value.src) {
            img.xcxUrl = value.temp;
          }
        }



        that.drawImg("shareCanvas", img, cardImgUrl, cardDate,j);


      }
    },function(res){
      console.log(res);
    })


  },
  drawImg: function (canvasid, item){
    var that = this;
    var title = item.channelName;
    var day = item.accumulateChannelDoCard;
    var cardUrl = item.cardImgUrl;
    var xcxUrl = item.xcxUrl;
    var avatarUrl = item.avatarUrl;
    var nickName = item.participantName;
    var cardDate = item.cardDate;
    if (nickName.length > 5) {
      nickName = nickName.substr(0, 5) + '...';
    }

    const ctx = wx.createCanvasContext(canvasid)
    ctx.setFillStyle('#fff')
    ctx.fillRect(0, 0, 255, 395);
    
    ctx.drawImage(cardUrl, 0, 0, 255, 310);
    ctx.drawImage(xcxUrl, 198, 316, 43, 43);

    ctx.setFontSize(14)
    ctx.setFillStyle('#333')
    ctx.fillText(title, 10, 330)

    ctx.setFontSize(10)
    ctx.setFillStyle('#999')
    ctx.fillText(nickName, 150, 375)
    ctx.fillText(cardDate, 192, 375)

    ctx.setFontSize(30)
    ctx.setFillStyle('#facc39')
    ctx.fillText(day, 10, 380)

    ctx.setFontSize(13)
    ctx.fillText('天', 55, 378)


    ctx.setFillStyle('rgba(0,0,0,0.1)');
    ctx.fillRect(0, 0, 255, 310);

    ctx.save();

    ctx.arc(130, 370, 10, 0, 2 * Math.PI);
    ctx.setFillStyle('#fff');
    ctx.fill();
    ctx.clip();
    ctx.drawImage(avatarUrl, 120, 360, 20, 20);
    ctx.restore();
    // var reserve = false;
    // if(index == 0){
    //   reserve = true;
    // }
    ctx.draw();
    ctx.draw(true, wx.canvasToTempFilePath({
        canvasId: canvasid,
        success: function (res) {
          var tempFilePath = res.tempFilePath;
          console.log(res);
          wx.saveImageToPhotosAlbum({
            filePath: tempFilePath,
            success: function (res) {
              wx.hideLoading();
              wx.showToast({
                title: '保存成功',
                icon: 'none',
                duration: 2000
              })
            }
          })

        },
        fail: function (res) {
          wx.hideLoading();
          console.log(res);
        }
      

      })
    )

    
  },
  changeImg:function(e){
    var that = this;
    var index = e.currentTarget.dataset.index;
    var weeklyDoCardMap = that.data.weeklyDoCardMap;
    var imgUrls = [];
    for (var i in weeklyDoCardMap) {
      var items = weeklyDoCardMap[i];
      var cardDayOfWeek = items.cardDayOfWeek;
      items.day = days[cardDayOfWeek - 1];
      var dailyDoCardList = items.dailyDoCardList;
      if (cardDayOfWeek == index+1) {
        for (var j in dailyDoCardList) {
          var v = dailyDoCardList[j];
          var cardImgUrl = items.cardImgUrl;
          var cardDate = items.cardDate;
          imgUrls.push({
            cardImgUrl: cardImgUrl,
            cardDate: cardDate,
            accumulateChannelDoCard: v.accumulateChannelDoCard,
            channelName: v.channelName,
            channelId: v.channelId,
            participantImgUrl: v.participantImgUrl,
            participantName: v.participantName,
            miniProgramQRCodeImgUrl: v.miniProgramQRCodeImgUrl
          });
        }
      }
    }

    that.setData({
      currentNavtab:0,
      imgUrls: imgUrls
    })
    

   
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // this.init();
    this.setData({
      showShareImg:true
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  }
})