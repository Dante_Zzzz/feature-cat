import util from '../../../../utils/util'

Page({
  didPressChooseImage: function () {
    var that = this;
    // 选择图片
    wx.chooseImage({
      count: 1,
      success: function (res) {
        var src = res.tempFilePaths[0];
        wx.navigateTo({
          url: `../upload/upload?src=${src}`
        })
      }
    })
  },
  /**
   * 页面的初始数据
   */
  data: {
    saveclass:'save',
    pid:'',
    name: '',
    imgUrl: '',
    imgHash: '',
    birthday: '',
    gender: '1',
    KTB_WXUSERNICKNAME: util.KTB_WXUSERNICKNAME,
    KTB_WXUSERAVATARURL: util.KTB_WXUSERAVATARURL
  },
  checkSaveClass() {
    var data = this.data;
    var saveclass = 'unsave'
    console.log(data);
    if (data.name && data.imgUrl && data.birthday && (data.gender == 0 || data.gender == 1)) {
      saveclass = 'save';
    }
    this.setData({
      saveclass: saveclass
    })
  },
  bindInputName: function (e) {
    var name = e.detail.value;
    this.setData({
      name: name.trim()
    })
    this.checkSaveClass();

  },
  changegender: function (e) {
    var gender = e.currentTarget.dataset.gender;
    this.setData({
      gender: gender
    })
  },
  bindDateChange: function (e) {
    this.setData({
      birthday: e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      pid: options.pid
    });
    this.loadData(options.pid);
  },
  loadData:function(pid){
    var that = this;
    util.wxRequest({
      url:'getParticipantInfo',
      data:{pid:pid},
      success:function(res){
        var participantInfo = res.participantInfo;
        that.setData({
          pid: participantInfo.pid,
          name: participantInfo.name,
          imgUrl: participantInfo.imgUrl,
          birthday: participantInfo.birthday,
          gender: participantInfo.gender
        })
      }
    });
  },
  init: function (imageURL, hash) {
    var that = this;
    that.setData({
      'imgUrl': imageURL,
      'imgHash': hash
    });
    that.checkSaveClass();
  },
  refreshPrevPage: function () {
    var pages = getCurrentPages();
    if (pages.length > 1) {
      var prePage = pages[pages.length - 2];
      prePage.init();
    }
  },
  del:function(){
    var that = this;
    var pid = this.data.pid;
    wx.showModal({
      title: '',
      content: '确定要删除吗',
      success: function (res) {
        if (res.confirm) {

          util.wxRequest({
            url: 'removeParticipant',
            data: { pid: pid },
            success: function (res) {
              that.refreshPrevPage();
              wx.navigateBack();
            }
          });
        } else if (res.cancel) {

        }
      }
    })


  },
  save: function () {
    var that = this;
    var data = this.data;
    var name = data.name;
    var imgUrl = data.imgUrl;
    var imgHash = data.imgHash;
    var gender = data.gender;
    var birthday = data.birthday;
    if (!imgUrl) {
      util.showToast('请上传头像');
      return;
    }
    if (!name) {
      util.showToast('请填写昵称');
      return;
    }
    var param = {
      name: name,
      gender: +gender,
      imgUrl: imgUrl,
      imgHash: imgHash,
      birthday: birthday,
      pid:data.pid
    };
    if (!birthday) {
      util.showToast('请选择生日');
      return;
    }
    util.wxRequest({
      url: 'modifyParticipantInfo',
      data: { 'participantInfo': param },
      success: function (res) {
        console.log(res);
        wx.showToast({
          title: '添加成功',
        })
        that.refreshPrevPage();
        wx.navigateBack();
      }
    });
  },
  cancel: function () {
    wx.navigateBack();
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  }
})