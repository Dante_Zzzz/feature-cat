const qiniuUploader = require("../../../common/libs/qiniuUploader");
import util from '../../../../utils/util';

Page({
  didPressChooseImage: function () {
    var that = this;
    // 选择图片
    wx.chooseImage({
      count: 1,
      success: function (res) {
        var src = res.tempFilePaths[0];
        wx.navigateTo({
          url: `../upload/upload?src=${src}`
        })
      
      }
    })
  },
  /**
   * 页面的初始数据
   */
  data: {
    saveclass:'unsave',
    name:'',
    imgUrl:'',
    imgHash:'',
    birthday:'',
    gender:''
  },
  touchStart(e) {
    this.wecropper.touchStart(e)
  },
  touchMove(e) {
    this.wecropper.touchMove(e)
  },
  touchEnd(e) {
    this.wecropper.touchEnd(e)
  },
  checkSaveClass(){
    var data = this.data;
    var saveclass = 'unsave'
    if (data.name && data.imgUrl && data.birthday && (data.gender == 0 || data.gender == 1)) {
      saveclass = 'save';
    }
    this.setData({
      saveclass: saveclass
    })
  },
  bindInputName:function(e){
    var name = e.detail.value;
    this.setData({
      name: name.trim()
    })
    this.checkSaveClass();
  
  },
  changegender:function(e){
    var gender = e.currentTarget.dataset.gender;
    this.setData({
      gender: gender
    })
    this.checkSaveClass();
  },
  bindDateChange:function(e){
    this.setData({
      birthday: e.detail.value
    })
    this.checkSaveClass()
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  save:function(e){
    console.log(e);
    var that = this;
    var data = this.data;
    if(data.saveclass == 'unsave'){
      return;
    }
    var name = data.name;
    var imgUrl = data.imgUrl;
    var imgHash = data.imgHash;
    var gender = data.gender;
    var birthday = data.birthday;
    if (!imgUrl) {
      util.showToast('请上传头像');
      return;
    }
    if (!name) {
      util.showToast('请填写昵称');
      return;
    }
    var param = {
      name:name,
      gender:+gender,
      imgUrl:imgUrl,
      imgHash:imgHash,
      birthday:birthday
    };
    if (!birthday) {
      util.showToast('请选择生日');
      return;
    } 
    if (!gender) {
      util.showToast('请选择性别');
      return;
    }
    util.wxRequest({ 
      url:'addParticipant',
      data:{'participantInfo':param},
      success:function(res){
        console.log(res);
        wx.showToast({
          title: '添加成功',
        })
        that.refreshPrevPage();
        wx.navigateBack();
    }});
  },
  refreshPrevPage:function(){
    var pages = getCurrentPages();
    if(pages.length>1){
      var prePage = pages[pages.length-2];
      prePage.init();
    }
  },
  cancel:function(){
    wx.navigateBack();
  },
  init: function (imageURL,hash){
    var that = this;
    that.setData({
      'imgUrl': imageURL,
      'imgHash':hash
    });
    that.checkSaveClass();
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  }
})