import util from '../../../../utils/util';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    doPluslList: [],
    pageInfo: {
      pageNum: 1,
      pageSize: 10
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    this.innerAudioContext = wx.createInnerAudioContext();
    this.innerAudioContext.onError((res) => {
      that.tip("播放录音失败！")
    })
    this.init();
  },
  
  deleteDoPlus: function(e) {
    var id = e.currentTarget.dataset.id;
    var that = this;
    wx.showModal({
      title: '',
      content: '确定要删除吗',
      success: function (res) {
        if (res.confirm) {
          util.wxRequest({
            url: 'deleteDoPlus',
            data: { doPlusId: id },
            success: function (res) {
              that.setData({
                pageInfo: {
                  pageNum: 1,
                  pageSize: 10
                }
              })
              that.init();
            }
          })

        } else if (res.cancel) {

        }
      }
    })

  },

  tip: function (msg) {
    wx.showModal({
      title: '提示',
      content: msg,
      showCancel: false
    })
  },
  playAudio: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var src = e.currentTarget.dataset.src;
    console.log(e)
    var contentList = this.data.topicList;
    // var isPlay = contentList[index].isPlay;
    // var duration = contentList[index].duration;

    that.innerAudioContext.src = src;
    that.innerAudioContext.onPlay(() => {
      console.log('开始播放');
      // contentList[index].isPlay = true;
      // that.setData({
      //   doPlusShowBaseInfoList: contentList
      // })
    })
    that.innerAudioContext.onPause(() => {
      console.log('暂停');
      // contentList[index].isPlay = false;
      // that.setData({
      //   doPlusShowBaseInfoList: contentList
      // })
    })
    that.innerAudioContext.onStop(() => {
      console.log('停止')
      // contentList[index].isPlay = false;
      // that.setData({
      //   doPlusShowBaseInfoList: contentList
      // })
    })
    that.innerAudioContext.onEnded(() => {
      console.log('结束')
      // contentList[index].isPlay = false;
      // contentList[index].progress = 100;
      // that.setData({
      //   doPlusShowBaseInfoList: contentList
      // })
    })


    // if (isPlay) {
    //   contentList[index].isPlay = false;
    //   that.setData({
    //     doPlusShowBaseInfoList: contentList
    //   })
    //   that.innerAudioContext.pause();
    // } else {
    that.innerAudioContext.play();
    //   that.innerAudioContext.onTimeUpdate(() => {
    //     var currentTime = that.innerAudioContext.currentTime;
    //     contentList[index].progress = currentTime * 1000 / duration * 100;
    //     contentList[index].nowduration = util.formarAudioTime(currentTime);
    //     that.setData({
    //       doPlusShowBaseInfoList: contentList
    //     })
    //   })
    // }
  },
  preImage: function (e) {
    var current = e.currentTarget.dataset.src;
    wx.previewImage({
      current: current, // 当前显示图片的http链接
      urls: [current] // 需要预览的图片http链接列表
    })
  },
  init: function () {
    var that = this;
    var pageInfo = that.data.pageInfo;
    util.wxRequest({
      url: 'getDoPlusList',
      data: { pageInfo: pageInfo },
      success: function (res) {
        var alldoPluslList = res.doPluslList;
        var pageInfo = res.pageInfo;
        pageInfo.pageNum++;
        if (pageInfo.pageNum > 2) {
          alldoPluslList = that.data.doPluslList.concat(alldoPluslList);
        }
        that.setData({
          doPluslList: alldoPluslList,
          pageInfo: pageInfo
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var pageInfo = that.data.pageInfo;
    if (pageInfo && pageInfo.size >= pageInfo.pageNum) {
      that.init();
    }
  }
})