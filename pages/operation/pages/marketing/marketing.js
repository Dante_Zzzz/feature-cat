import util from '../../../../utils/util';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    marketingItemList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.init();
  },
  bindInputHandle:function(e){
    var that = this;
    var value = e.detail.value.trim();
    var index = e.currentTarget.dataset.index;
    var marketingItemList = that.data.marketingItemList;
    marketingItemList[index].marketingItemId = value;
    that.setData({
      marketingItemList: marketingItemList
    })

  },
  bindInputCNameHandle: function (e) {
    var that = this;
    var value = e.detail.value.trim();
    var index = e.currentTarget.dataset.index;
    var marketingItemList = that.data.marketingItemList;
    marketingItemList[index].itemName = value;
    that.setData({
      marketingItemList: marketingItemList
    })

  },
  
  init:function(){
    var that = this;
    util.wxRequest({
      url: 'getMarketingItemList',
      success: function (res) {
        
        that.setData({
          marketingItemList: res.marketingItemList
        })
      }
    })
  },
  edit:function(e){
    var that = this;
    var index = e.currentTarget.dataset.index;
    var marketingItemList = that.data.marketingItemList;
    marketingItemList[index].isEdit = true;
    that.setData({
      marketingItemList: marketingItemList
    })
  },
  save:function(e){
    var that = this;
    var index = e.currentTarget.dataset.index;
    var marketingItemList = that.data.marketingItemList[index];
    util.wxRequest({
      url: 'updateMarketingItem',
      data: { marketingItem: marketingItemList },
      success: function (res) {
        that.init();
      }
    })
  },
  delitem:function(e){
    var id = e.currentTarget.dataset.id;
    var that = this;

    wx.showModal({
      title: '',
      content: '确定要删除吗',
      success: function (res) {
        if (res.confirm) {
          util.wxRequest({
            url: 'deleteMarketingItem',
            data: { marketingItemId: id },
            success: function (res) {
              that.init();
            }
          })

        } else if (res.cancel) {

        }
      }
    })

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  }
})