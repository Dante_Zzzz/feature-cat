import util from '../../../../utils/util';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    doCardTemplateList:[],
    pageInfo: {
      pageNum: 1,
      pageSize: 10
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.init();
  },
  init: function () {
    var that = this;
    var pageInfo = that.data.pageInfo;
    util.wxRequest({
      url: 'getDoCardList',
      data: { pageInfo: pageInfo},
      success: function (res) {
        var pageInfo = res.pageInfo;
        pageInfo.pageNum++;

        var alldoCardTemplateList = res.doCardTemplateList;
        if (pageInfo.pageNum > 2) {
          alldoCardTemplateList = that.data.doCardTemplateList.concat(alldoCardTemplateList);
        }
        that.setData({
          doCardTemplateList: alldoCardTemplateList,
          pageInfo: pageInfo
        })
      }
    })
  },
  goadddocard:function(){
    wx.navigateTo({
      url: '../adddocard/adddocard'
    })
  },
  delcard:function(e){
    var id = e.currentTarget.dataset.id;
    var that = this;

    wx.showModal({
      title: '',
      content: '确定要删除吗',
      success: function (res) {
        if (res.confirm) {
          util.wxRequest({
            url: 'deleteDoCard',
            data: { doCardTemplateId: id },
            success: function (res) {
              that.setData({
                pageInfo: {
                  pageNum: 1,
                  pageSize: 10
                }
              })
              that.init();
            }
          })
          
        } else if (res.cancel) {

        }
      }
    })
   
  },
  preImage: function (e) {
    var current = e.currentTarget.dataset.src;
    wx.previewImage({
      current: current, // 当前显示图片的http链接
      urls: [current] // 需要预览的图片http链接列表
    })
  },
  onReachBottom: function () {
    var that = this;
    var pageInfo = that.data.pageInfo;
    if (pageInfo && pageInfo.size >= pageInfo.pageNum) {
      that.init();
    }
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  }
})