import util from '../../../../utils/util';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    saveclass:'unsave',
    cardImgUrl:'',
    cardDate: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },
  chooseImg: function () {
    var that = this;
    wx.chooseImage({
      count: 1,
      success: function (res) {
        var src = res.tempFilePaths[0];
        wx.showLoading();
        util.upload(src, function (res) {
          that.setData({
            'cardImgUrl': res.imageURL,
          });
          that.checkSaveClass();
          wx.hideLoading();
        }, function () {
          wx.hideLoading();
        });

      }

    })
  },
  bindDateChange: function (e) {
    this.setData({
      cardDate: e.detail.value
    })
    this.checkSaveClass()
  },
  checkSaveClass:function(){
    var cardImgUrl = this.data.cardImgUrl;
    var cardDate = this.data.cardDate;
    var saveclass = 'unsave';
    if (cardDate && cardImgUrl) {
      saveclass = 'save';
    } 

    this.setData({
      saveclass: saveclass
    })
  },
  save:function(){
    var that = this;
    var saveclasss = this.data.saveclasss;
    if (saveclasss == 'unsave') {
      return;
    }
    var cardDate = that.data.cardDate;
    var cardImgUrl = that.data.cardImgUrl;
    util.wxRequest({
      url: 'addDoCard',
      data: { doCard: { cardImgUrl: cardImgUrl, cardDate: cardDate} },
      success: function (res) {
        wx.navigateBack({
          
        })
      },onfail:function(res){
        if(res && res.code){
          util.showToast(res.message);
        }
      }
    })
  },
  cancel:function(){
    wx.navigateBack({
      
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  }
})