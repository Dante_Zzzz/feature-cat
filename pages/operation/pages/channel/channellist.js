import util from '../../../../utils/util';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    channelList:[],
    newChannel:'',
    pageInfo: {
      pageNum: 1,
      pageSize: 10
    }
  
  },
  recommendChannel:function(e){
    var id = e.currentTarget.dataset.id;
    var recommend = e.currentTarget.dataset.recommend;
    var isrecommend = e.currentTarget.dataset.isrecommend;
    if (isrecommend == recommend){
      return;
    }
    var that = this;
    util.wxRequest({
      url: 'recommendChannel',
      data: { isRecommend: !recommend, channelId:id },
      success: function (res) {
        that.setData({
          pageInfo: {
            pageNum: 1,
            pageSize: 10
          }
        })
        that.init();
      }
    })
  },
  deleteChannel:function(e){
    var id = e.currentTarget.dataset.id;
    var that = this;
    wx.showModal({
      title: '',
      content: '确定要删除吗',
      success: function (res) {
        if (res.confirm) {
          util.wxRequest({
            url: 'deleteChannel',
            data: { channelId: id },
            success: function (res) {
              that.setData({
                pageInfo: {
                  pageNum: 1,
                  pageSize: 10
                }
              })
              that.init();
            }
          })

        } else if (res.cancel) {

        }
      }
    })

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.init();
  },
  init:function(){
    var that = this;
    var pageInfo = that.data.pageInfo;
    util.wxRequest({
      url: 'getChannelList',
      data: { pageInfo: pageInfo },
      success: function (res) {
        var pageInfo = res.pageInfo;
        pageInfo.pageNum++;
        var allchannelList = res.channelList;
        if (pageInfo.pageNum > 2) {
          allchannelList = that.data.channelList.concat(allchannelList);
        }
        var totalChannel = res.pageInfo.total;
        that.setData({
          totalChannel: totalChannel,
          newChannel: res.newChannel,
          channelList: allchannelList,
          pageInfo: pageInfo
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var pageInfo = that.data.pageInfo;
    if (pageInfo && pageInfo.size >= pageInfo.pageNum) {
      that.init();
    }
  }
})