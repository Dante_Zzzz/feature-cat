// pages/operation/pages/index/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },
  gotopic:function(){
    wx.navigateTo({
      url: '../topic/topic'
    })
  },
  gochannellist:function(){
    wx.navigateTo({
      url: '../channel/channellist'
    })
  },
  godocard:function(){
    wx.navigateTo({
      url: '../docard/docard'
    })
  },
  gomarketing:function(){
    wx.navigateTo({
      url: '../marketing/marketing'
    })
  },
  godolist:function(e){
    wx.navigateTo({
      url: '../dolist/dolist'
    })
  },
  gogrey: function (e) {
    wx.navigateTo({
      url: '../grey/grey'
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  }

 
})