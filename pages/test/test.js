var app = getApp()

Page({
  data: {
    audioPlayer: null
  },
  onLoad() {
    this.setData({
      audioPlayer: app.globalData.audioPlayer
    })
  },

  onTap: function () {
    // var audioPlayer = wx.getBackgroundAudioManager()
    var audioPlayer = this.data.audioPlayer
    audioPlayer.src = 'https://media.talentboom.cn/tmp_89758386b133111b2748fde6d000b06a.m4a'
  }
})