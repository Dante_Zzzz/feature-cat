import util from '../../../../utils/util';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    from:'',
    topicInfoDTO:{},
    rjpheight:0,
    channelDoPlusCount:'',
    channelMemberCount:'',
    commentZone:{},
    channelBaseInfo:{},
    doPlusShowBaseInfo:{},
    isShowComment:false,
    comment:'',
    toUserId:'',
    commentId:'',
    commentPlaceholder:'输入评论'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showShareMenu({
      withShareTicket: true
    });
    this.setData({
      doPlusId: options.doPlusId,
      from: options.from
    })
    this.init();
    this.innerAudioContext = wx.createInnerAudioContext();
    this.innerAudioContext.onError((res) => {
      that.tip("播放录音失败！")
    })
    this.setData({
      comment: wx.getStorageSync('comment')
    })
  },

  init:function(){
    var that = this;
    var doPlusId = that.data.doPlusId;
    util.wxRequest({
      url:'getDoPlusDetailPageInfo',
      data: { doPlusId: doPlusId},
      success:function(res){
        var doPlusShowBaseInfo = res.doPlusShowBaseInfo;
        var textContentPreview = doPlusShowBaseInfo.textContentPreview;
        var textContent = doPlusShowBaseInfo.doPlusContent.textContent;
        if (textContentPreview && textContent && textContentPreview != textContent) {
          doPlusShowBaseInfo.showall = true;
        }
        if (doPlusShowBaseInfo.doPlusContent.soundContent) {
          var thumb = doPlusShowBaseInfo.doPlusContent.soundContent.soundSeconds / 1000;
          var formatduration = util.formarAudioTime(thumb);
          doPlusShowBaseInfo.nowduration = '0"';
          doPlusShowBaseInfo.duration = thumb * 1000;
          doPlusShowBaseInfo.formatduration = formatduration;
          doPlusShowBaseInfo.context = doPlusShowBaseInfo.doPlusContent.soundContent.soundUrl;
        }
        if (doPlusShowBaseInfo.doPlusContent.videoContent) {
          var videoWidth = doPlusShowBaseInfo.doPlusContent.videoContent.videoWidth;
          var videoHeight = doPlusShowBaseInfo.doPlusContent.videoContent.videoHeight;
          var videoSize = util.getVideoSize(videoWidth, videoHeight);
          videoWidth = videoSize.videoWidth;
          videoHeight = videoSize.videoHeight;
          doPlusShowBaseInfo.doPlusContent.videoContent.videoWidth = videoWidth;
          doPlusShowBaseInfo.doPlusContent.videoContent.videoHeight = videoHeight;

        }



        var channelBaseInfo = res.doPlus.channelBaseInfo;
        that.setData({
          channelMemberCount: res.channelMemberCount,
          channelDoPlusCount: res.channelDoPlusCount,
          commentZone: res.commentZone,
          channelBaseInfo:channelBaseInfo,
          doPlusShowBaseInfo:doPlusShowBaseInfo,
          topicInfoDTO:res.topicInfoDTO
        })
      }
    })
  },
  showall: function (e) {
    var doPlusShowBaseInfo = this.data.doPlusShowBaseInfo;
    doPlusShowBaseInfo.showall = false;
    doPlusShowBaseInfo.textContentPreview =          doPlusShowBaseInfo.doPlusContent.textContent;
    this.setData({
      doPlusShowBaseInfo: doPlusShowBaseInfo
    })
  },
  likeDoPlus:function(e){
    var that = this;
    var data = that.data;
    var islikeDoPlus = !e.currentTarget.dataset.ilike;
    var likeDoPlusDTO = {
      doPlusId: data.doPlusId,
      islikeDoPlus: islikeDoPlus
    };
    util.wxRequest({
      url:'likeDoPlus',
      data: { likeDoPlusDTO: likeDoPlusDTO},
      success:function(res){
        that.init();
      }
    })
  },
  bindinputComment:function(e){
    var value = e.detail.value.trim();
    this.setData({
      comment: value
    })
    wx.setStorageSync('comment', value);
  },
  bindconfirmComment:function(){
    this.setData({
      isShowComment: false
    })
    this.commentDoPlus();
  },
  showCommentHandle: function () {
    var isShowComment = this.data.isShowComment;
    var commentPlaceholder = '输入评论';
    this.setData({
      commentId: '',
      toUserId: '',
      isShowComment: !isShowComment,
      commentPlaceholder: commentPlaceholder,
      comment:wx.getStorageSync('comment')
    })
  },
  showComment: function (){
    var isShowComment = this.data.isShowComment;
    var commentPlaceholder = this.data.commentPlaceholder || '输入评论';
    this.setData({
      commentId:'',
      toUserId:'',
      isShowComment: !isShowComment,
      commentPlaceholder: commentPlaceholder
    })

  },
  getUserInfoByDolikeHandler: function (e) {
    var that = this;
    var userInfo = e.detail.userInfo;
    util.setUserInfoHandler(userInfo, function () {
    });
  },
  getUserInfoByCommentHandler: function (e) {
    var that = this;
    var userInfo = e.detail.userInfo;
    util.setUserInfoHandler(userInfo, function () {
    });
  },
  bindblurHandle:function(){
    var isShowComment = this.data.isShowComment;
    var commentPlaceholder = this.data.commentPlaceholder || '输入评论';
    this.setData({
      rjpheight:0,
      commentId: '',
      toUserId: '',
      isShowComment: false,
      commentPlaceholder: commentPlaceholder
    })
  },
  bindfocusHandle:function(e){
    // var that = this;
    // var rjpheight = e.detail.height;
    // setTimeout(function(){
      // that.setData({
      //   rjpheight: rjpheight || 0
      // })
    // },200)
    
    // wx.createSelectorQuery().select('#comment_page').boundingClientRect(function (rect) {
    //   // 使页面滚动到底部
    //   wx.pageScrollTo({
    //     scrollTop: rect.bottom
    //   })
    // }).exec()
  },
  gohome:function(){
    wx.switchTab({
      url: '/pages/tabBar/discover/index'
    })
  },
  commentDoPlus:function(){
    var that = this;
    var data = that.data;
    var comment = data.comment;
    if (!comment){
      return;
    }
    var comment = {
      doPlusId: data.doPlusId,
      comment: comment
    };
    var toUserId = data.toUserId;
    if (toUserId){
      comment.toUserId = toUserId;
      comment.commentId = data.commentId;
    }
    var operation = 0;
    util.wxRequest({
      url: 'commentDoPlus',
      data: { operation: operation, comment: comment },
      success: function (res) {
        wx.removeStorageSync('comment');
        that.init();
      }
    })
  },
  delCommentDoPlus: function () {
    var that = this;
    var data = that.data;
    var comment = {
      doPlusId: data.doPlusId,
      commentId:data.commentId
    }
    var operation = 1;
    util.wxRequest({
      url: 'commentDoPlus',
      data: { operation: operation, comment: comment },
      success: function (res) {
        that.init();
      }
    })
  },
  playVedio: function (e) {
    var that = this;
    var doPlusShowBaseInfo = this.data.doPlusShowBaseInfo;
    doPlusShowBaseInfo.showVedio = true;
    that.setData({
      doPlusShowBaseInfo: doPlusShowBaseInfo
    })
    that.stopAudio();
  },
  tip: function (msg) {
    wx.showModal({
      title: '提示',
      content: msg,
      showCancel: false
    })
  },
  stopAudio:function(){
    var that = this;
    that.innerAudioContext.pause();
    var doPlusShowBaseInfo = that.data.doPlusShowBaseInfo;
    doPlusShowBaseInfo.isPlay = false;
    that.setData({
      doPlusShowBaseInfo: doPlusShowBaseInfo
    })
  },
  playAudio: function (e) {
    var that = this;
    var doPlusShowBaseInfo = this.data.doPlusShowBaseInfo;
    var isPlay = doPlusShowBaseInfo.isPlay;
    var duration = doPlusShowBaseInfo.duration;

    var src = doPlusShowBaseInfo.context;

    that.innerAudioContext.src = src;
    that.innerAudioContext.onPlay(() => {
      console.log('开始播放');
      doPlusShowBaseInfo.isPlay = true;
      that.setData({
        doPlusShowBaseInfo: doPlusShowBaseInfo
      })
    })
    that.innerAudioContext.onPause(() => {
      console.log('暂停');
      doPlusShowBaseInfo.isPlay = false;
      that.setData({
        doPlusShowBaseInfo: doPlusShowBaseInfo
      })
    })
    that.innerAudioContext.onStop(() => {
      console.log('停止')
      doPlusShowBaseInfo.isPlay = false;
      that.setData({
        doPlusShowBaseInfo: doPlusShowBaseInfo
      })
    })
    that.innerAudioContext.onEnded(() => {
      console.log('结束')
      doPlusShowBaseInfo.isPlay = false;
      doPlusShowBaseInfo.progress = 100;
      that.setData({
        doPlusShowBaseInfo: doPlusShowBaseInfo
      })
    })



    if (isPlay) {
      doPlusShowBaseInfo.isPlay = false;
      that.setData({
        doPlusShowBaseInfo: doPlusShowBaseInfo
      })
      that.innerAudioContext.pause();
    } else {
      that.innerAudioContext.play();
      that.innerAudioContext.onTimeUpdate(() => {
        var currentTime = that.innerAudioContext.currentTime;
        doPlusShowBaseInfo.progress = currentTime * 1000 / duration * 100;
        doPlusShowBaseInfo.nowduration = util.formarAudioTime(currentTime);
        that.setData({
          doPlusShowBaseInfo: doPlusShowBaseInfo
        })
      })
    }
  },
  recomment: function (e) {
    var name = e.currentTarget.dataset.name;
    var uid = e.currentTarget.dataset.uid;
    var commentId = e.currentTarget.dataset.commentid;
    var isCommentOwner = e.currentTarget.dataset.iscommentowner;
    var that = this;
    var itemList = ['回复']
    if (isCommentOwner){
      itemList.push('删除');
    }else{
      that.showComment();
      that.setData({
        commentId: commentId,
        toUserId: uid,
        commentPlaceholder: '回复:@' + name
      })
      return;
    }
    wx.showActionSheet({
      itemList: itemList,
      itemColor: '#333',
      success: function (res) {
        var index = res.tapIndex;
        if (index == 0) {
          that.showComment();
          that.setData({
            commentId: commentId,
            toUserId: uid,
            commentPlaceholder: '回复:@' + name
          })
        } else {
          that.setData({
            commentId: commentId,
            toUserId: uid,
          })
          that.delCommentDoPlus()
        }
      },
      fail: function (res) {
        console.log(res.errMsg)
      }
    })

  },
  preImage: function (e) {
    var current = e.currentTarget.dataset.src;
    var urls = this.data.doPlusShowBaseInfo.doPlusContent.imageContent;
    wx.previewImage({
      current: current, // 当前显示图片的http链接
      urls: urls // 需要预览的图片http链接列表
    })
  },

  backHandle: function () {
    var from = this.data.from;
    wx.navigateBack({
      delta: from === 'channel' ? 1:2
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '动态',
      path: 'pages/home/pages/detail/detail?doPlusId=' + this.data.doPlusId,
      success: function (res) {
        // 转发成功
        console.log('onShareAppMessage');
        console.log(res.shareTickets);
        if (res.shareTickets) {
          wx.getShareInfo({
            shareTicket: res.shareTickets[0],
            success(res) {
              console.log(res);
              var encryptedData = res.encryptedData;
              var iv = res.iv;
              util.wxRequest({
                url: 'updateGroupInfo',
                data: { encryptedData: encryptedData, iv: iv },
                success: function (res) {
                  console.log(res);
                }
              })
            }
          })
        }
      },
      fail: function (res) {
        // 转发失败
      }
    }
  }

})