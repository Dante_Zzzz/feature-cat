import util from '../../../../utils/util';
const _ = require('../../../common/libs/lodash.core.min.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    videoClass:'hide',
    doclass:'undo',
    endaudio:false,
    timer:0,
    audioTime:'',
    topicId:'',
    topicTitle:'',
    topicOrder:'',
    isPublic:true,
    textContent:'',
    soundContent:{},
    videoContent:{},
    imageContent:[],
    channelId:'',
    hasSoundContent:false,
    hasVideoContent:false,
    nowduration:'',
    formatduration:'',
    isPlay: false,
    progress: 0, 
    audioTempFilePath:''
  },
  cancel:function(){
    wx.navigateBack()
  },
  getUserInfoHandler: util.throttle(function (e) {
    var that = this;
    var doclass = that.data.doclass;
    if (doclass === 'undo') {
      return;
    }
    var userInfo = e.detail.userInfo;
    util.setUserInfoHandler(userInfo, function () {
      that.doplus(e);
    });
  }, 2000),
  goplaydetail:function(){
    var topicId = this.data.topicId;
    var channelId = this.data.channelId;
    wx.navigateTo({
      url: '../../../discover/pages/channelinfo/playdetail?from=do&topicId=' + topicId + '&channelId=' + channelId
    })
  },
  delImg:function(e){
    var index = e.currentTarget.dataset.index;
    var imageContent = this.data.imageContent;
    imageContent.splice(index, 1);
    console.log(imageContent)
    this.setData({
      imageContent: imageContent
    })
    
  },
  delVideo:function(){
    this.setData({
      hasVideoContent:false,
      videoContent:{}
    })
  },
  playVideo:function(){
    this.videoContext.play();
    this.videoContext.requestFullScreen();
    this.setData({
      videoClass:'show'
    })
  },
  videoFullscreenchange:function(e){
    var fullScreen = e.detail.fullScreen;
    var videoClass = fullScreen ? 'show' :'hide';
    if (!fullScreen){
      this.videoContext.pause();
    }
    this.setData({
      videoClass: videoClass
    })
  },
  delaudio:function(){
    this.setData({
      hasSoundContent:false,
      endaudio:false,
      timer:0,
      audioTime:'',
      progress: 0,
      nowduration: '',
      audioTempFilePath:'',
      soundContent:{}
    })
  },
  formatTime: function (audioTime){
    console.log(audioTime);
    var minute = Math.floor(audioTime / 60 % 60);
    var second = Math.floor(audioTime % 60);
    if (minute < 10) {
      minute = "0" + minute;
    }
    if (second < 10) {
      second = "0" + second;
    }
    console.log(minute + ':' + second);

    return minute +':'+ second;
  },

  addaudio:function(){
    var timer;
    var that = this;
    var hasSoundContent = this.data.hasSoundContent;
    const recorderManager = wx.getRecorderManager();
    recorderManager.onStart(() => {
      console.log('recorder start')
      that.setData({
        hasSoundContent: true
      })
      that.timer = setInterval(function () {
        var timer = that.data.timer;
        var audioTime = that.data.audioTime;
        that.setData({
          timer: timer+1,
          audioTime: that.formatTime(timer+1)
        })
      }, 1000);
    })
    recorderManager.onResume(() => {
      console.log('recorder resume')
    })
    recorderManager.onPause(() => {
      console.log('recorder pause')
    })
    recorderManager.onStop((res) => {
      console.log('recorder stop', res)
      clearInterval(that.timer);
      that.upload(2, res.tempFilePath, res);
      that.setData({
        audioTempFilePath: res.tempFilePath,
        endaudio:true,
      })
    })
    recorderManager.onFrameRecorded((res) => {
      const { frameBuffer } = res
      console.log('frameBuffer.byteLength', frameBuffer.byteLength)
    })

    const options = {
      duration: 600000,
      sampleRate: 44100,
      numberOfChannels: 1,
      encodeBitRate: 192000,
      format: 'aac',
      frameSize: 50
    }
    if (hasSoundContent) {
      recorderManager.stop();
    } else {
      recorderManager.start(options)
    }
  },
  playAudio: function (e) {
    var that = this;
    var isPlay = that.data.isPlay;
    var duration = that.data.soundContent.soundSeconds;
    var src = that.data.audioTempFilePath;
    
    that.innerAudioContext.src = src;
    that.innerAudioContext.onPlay(() => {
      console.log('开始播放');
      that.setData({
        isPlay: true
      })
    })
    that.innerAudioContext.onPause(() => {
      console.log('暂停');
      that.setData({
        isPlay: false
      })
    })
    that.innerAudioContext.onStop(() => {
      console.log('停止')
      that.setData({
        isPlay: false
      })
    })
    that.innerAudioContext.onEnded(() => {
      console.log('结束')
      that.setData({
        isPlay:false,
        progress: 100,
      })
    })


    if (isPlay) {
      that.innerAudioContext.pause();
    } else {
      that.innerAudioContext.play();
      that.innerAudioContext.onTimeUpdate(() => {
        var currentTime = that.innerAudioContext.currentTime;
        var progress = currentTime * 1000 / duration * 100;
        var nowduration = util.formarAudioTime(currentTime);
        that.setData({
          progress: progress,
          nowduration: nowduration
        })
      })
    }
  },
  getVedioWeithAndHeight:function(src,success){
      wx.request({
        url: src+'?avinfo',
        success:function(res){

        }
      });
  },
  upload: function (type, src, res) {
    var that = this;
    wx.showLoading();
    util.upload(src, function(resp){
      if (type == 1 || type == 3) {
        var src = resp.imageURL
        var videoContent = { videoUrl: src};

        that.setData({
          hasVideoContent:true,
          videoContent: videoContent,
        })
        wx.hideLoading();
        that.checkSaveClass();

        // wx.request({
        //   url: src+'?avinfo',
        //   success:function(res){
        //     console.log(res);
        //     var vwidth = 320;
        //     var vheight = 182;
        //     if(res && res.data && res.data.streams && res.data.streams.length){
        //       var streams = res.data.streams;
        //       vwidth= streams[0].width/3 || streams[1].width/1.5;
        //       vheight = streams[0].height/3 || streams[1].height/1.5;
        //     }
        //     that.setData({
        //       hasVideoContent:true,
        //       videoContent: videoContent,
        //       vheight:vheight,
        //       vwidth:vwidth
        //     })

        //     wx.hideLoading();
        //     that.checkSaveClass();

        //   },complete:function(){
        //     wx.hideLoading();
        //   }
        // });
      } else if (type == 2) {
        var duration = res.duration;
        var thumb = Math.ceil(duration / 1000);
        var formatduration = util.formarAudioTime(thumb);
        var soundContent = { soundUrl: resp.imageURL, soundSeconds: duration}
        var nowduration = '0"';
        that.setData({
          soundContent: soundContent,
          nowduration: nowduration,
          formatduration: formatduration
        })
        wx.hideLoading();
      }
      that.checkSaveClass();
    }, function(){
      wx.hideLoading();
    });

  },
  addvedio:function(){
    var that = this;
    wx.chooseVideo({
      sourceType: ['album', 'camera'],
      maxDuration: 60,
      camera: 'back',
      success: function (res) {
        // if(res && res.size && res.size > 10*1024*1024){
        if(res && res.size && res.size > 10*1024*1024){
          util.showToast('文件过大，请重新上传');
          return;
        }
        var src = res.tempFilePath;
        that.upload(3, src);
      }
    })
  },
  bindInputTextContent:function(e){
    var value = e.detail.value.trim();
    this.setData({
      textContent: value,
    })
    this.checkSaveClass();
  },
  preImage:function(e){
    var current = e.currentTarget.dataset.src;
    var urls = this.data.imageContent;
    wx.previewImage({
      current: current, // 当前显示图片的http链接
      urls: urls // 需要预览的图片http链接列表
    })
  },
  refreshPrevPage: function () {
    var pages = getCurrentPages();
    if (pages.length > 1) {
      var prePage = pages[pages.length - 2];
      prePage.init();
    }
  },
  doplus: util.throttle(function (e) {
    var that = this;
    var data = that.data;
    var doPlusInfo = {
      channelId: data.channelId,
      textContent: data.textContent,
      isPublic: data.isPublic,
      topicId: data.topicId || ''
    };
    if (data.imageContent){
      doPlusInfo.imageContent = data.imageContent;
    }
    if (!_.isEmpty(data.videoContent)) {
      doPlusInfo.videoContent = data.videoContent;
    }
    if (!_.isEmpty(data.soundContent)) {
      doPlusInfo.soundContent = data.soundContent;
    }
    util.wxRequest({
      url:'submitDo',
      data: { doPlusInfo: doPlusInfo},
      success:function(res){
        // that.refreshPrevPage();
        wx.navigateBack()
      }
    })
  }, 2000),
  checkSaveClass:function(){
    var data = this.data;
    var doclass = 'undo';
    if (data.textContent || (data.imageContent && data.imageContent.length) || !_.isEmpty(data.soundContent) || data.hasVideoContent ) {
      doclass = 'doplus';
    }
    this.setData({
      doclass: doclass
    })
  },
  uploadImg:function(src){
    var that = this;
    var imageContent = that.data.imageContent;

    wx.showLoading();
    util.upload(src, function(res){
      imageContent.push(res.imageURL);
      that.setData({
        'imageContent': imageContent,
      });
      that.checkSaveClass();
      wx.hideLoading();
    }, function(){
      wx.hideLoading();
    });

  },
  chooseImg: function () {
    var that = this;
    var imageContent = this.data.imageContent;
    if (imageContent.length > 9){
      wx.showToast({
        title: '最多能上传9张哦',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    // 选择图片
    wx.chooseImage({
      count: 9,
      success: function (res) {
        var src = res.tempFilePaths;
        wx.showLoading();
        var arr = [];
        if (imageContent.length + src.length > 9){
          arr = src.slice(0, 9 - imageContent.length);
        }else{
          arr = src;
        }
        for (var i = 0; i < arr.length;i++){
          that.uploadImg(arr[i]);
        }
        wx.hideLoading();
      }

    })
  },
  chooseType: function () {
    var that = this;
    wx.showActionSheet({
      itemList: ['公开', '仅自己可见'],
      itemColor: '#333',
      success: function (res) {
        var index = res.tapIndex;
        if(index == 0){
          that.setData({
            isPublic:true
          })
        }else{
          that.setData({
            isPublic: false
          })
        }
      },
      fail: function (res) {
        console.log(res.errMsg)
      }
    })

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      channelId: options.channelId,
      topicId: options.topicId,
      topicOrder: options.topicOrder,
      topicTitle: options.topicTitle
    })
    this.innerAudioContext = wx.createInnerAudioContext();
    this.innerAudioContext.onError((res) => {
      that.tip("播放录音失败！")
    })
    this.videoContext = wx.createVideoContext('myVideo')
  },
  tip: function (msg) {
    wx.showModal({
      title: '提示',
      content: msg,
      showCancel: false
    })
  },
  // init: function () {
  //   var that = this;
  //   var channelId = this.data.channelId;
  //   util.wxRequest({
  //     url: 'getDoPageInfo',
  //     data: { channelId: channelId },
  //     success: function (res) {
  //       var topic = res.topic;

  //       that.setData({
  //         topic: topic,
  //         topicId: topic.topicId || ''
  //       })

  //     }
  //   })
  // },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  }
})