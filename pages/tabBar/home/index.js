import util from '../../../utils/util';
const _ = require('../../common/libs/lodash.core.min.js');
Page({
  /**
   * 页面的初始数据
   */
  data: {
    show:false,
    readPicBookDisplayItemList:[],
    pageInfo: {
      pageNum: 1,
      pageSize: 10
    }
  },
  getUserInfoHandler: function (e) {
    var that = this;
    var userInfo = e.detail.userInfo;
    util.setUserInfoHandler(userInfo,function(){
      that.gocreatechannel();
    });
  },
  godoplus: function (e) {
    var topic;
    var channelId = e.currentTarget.dataset.channelid;
    var url = '../../home/pages/do/do?channelId=' + channelId;
    if (topic) {
      url += '&topidId=' + topic.topicId;
    }
    wx.navigateTo({
      url: url
    })

  },
  gocreatechannel: function () {
    wx.navigateTo({
      url: '../../discover/pages/createchannel/createchannel'
    })
  },
  gochannelsetting:function(e){
    wx.navigateTo({
      url: '../../discover/pages/channelsetting/channelsetting'
    })
  },
  gotopic: function (e) {
    var channelId = e.currentTarget.dataset.channelid;
    var topicId = e.currentTarget.dataset.topicid;
    wx.navigateTo({
      url: '../../discover/pages/channelinfo/playdetail?channelId=' + channelId + '&topicId=' + topicId
    })
  },
  gochannelinfo:function(e){
    var channelId = e.currentTarget.dataset.channelid;
    wx.navigateTo({
      url: '../../discover/pages/channelinfo/channelinfo?channelId=' + channelId
    })
  },
  exist: function (obj, pid) {
    var flag = false;
    _.each(obj, function (item, i) {
      if (pid == i) {
        flag = true;
      }
    })
    return flag;
  },
  groupBy: function (data) {
    var that = this;
    var obj = {};
    var notfinishList = [];
    var picList = [];
    _.each(data, function (item, i) {
      if (item.isDoPlusFinished){
        var pid = item.participantInfo.pid;
        if (!that.exist(obj, pid)) {
          obj[pid] = [item];
          picList[pid] = item.participantInfo.imgUrl;
        } else {
          var tmp = obj[pid];
          tmp.push(item)
          obj[pid] = tmp;
        }
      }else{
        notfinishList.push(item);
      }
    })
    var finishList = [];
    for (var i in obj) {
      finishList.push({ imgUrl: picList[i], data: obj[i] })
    }
    return { finishList: finishList, notfinishList: notfinishList};
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  init:function(){
    var that = this;
    var isHideLoading = false;
    if(that.data.show){
      isHideLoading = true;
    }
    var pageInfo = that.data.pageInfo;
    util.wxRequest({
      url:'getPlayPicBookPageInfo',
      data: pageInfo,
      success:function(res){
        var pageInfo = res.pageInfo || {};
        pageInfo.pageNum++;
        var readPicBookDisplayItemList = res.readPicBookDisplayItemList;

        var allreadPicBookDisplayItemList = readPicBookDisplayItemList;
        if (pageInfo.pageNum > 2) {
          allreadPicBookDisplayItemList = that.data.readPicBookDisplayItemList.concat(readPicBookDisplayItemList);
        }
          that.setData({
            readPicBookDisplayItemList: allreadPicBookDisplayItemList,
            show: true
          })

      },
      onfail:function(){

      },
      isHideLoading:isHideLoading
    })
  },
  
  godiscover:function(){
    wx.switchTab({
      url: '/pages/tabBar/discover/index'
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    // wx.showTabBarRedDot({
    //   index:2
    // });
 
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    that.setData({
      pageInfo: {
        pageNum: 1,
        pageSize: 10
      }
    });
    var token = wx.getStorageSync('token');
    if(token){
      that.init();
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.init();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var pageInfo = that.data.pageInfo;
    if (pageInfo && pageInfo.size >= pageInfo.pageNum) {
      that.init();
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})
