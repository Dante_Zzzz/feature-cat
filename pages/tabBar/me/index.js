import util from '../../../utils/util'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    show:false,
    userInfo:{},
    unReadMsgCount: 0,
    participantList:[],
    KTB_WXUSERNICKNAME:'KTB_WXUSERNICKNAME',
    KTB_WXUSERAVATARURL:'KTB_WXUSERAVATARURL'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },
  init:function(){
    var that = this;
    var isHideLoading = false;
    if(that.data.show){
      isHideLoading = true;
    }
    util.wxRequest({
      url:'getMinePageInfo',
      success:function(res){
        var unReadMsgCount = res.unReadMsgCount;
        that.setData({
          unReadMsgCount: unReadMsgCount,
          participantList: res.participantList,
          show:true
        }) 
        if (unReadMsgCount == 0){
          wx.hideTabBarRedDot({index:2});
        } else if (unReadMsgCount > 0){
          wx.showTabBarRedDot({
            index: 2
          })
        }
      },
      onfail:function(){

      },
      isHideLoading:isHideLoading
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    // wx.hideTabBarRedDot({
    //   index: 2
    // });
  },
  godolist:function(){
    wx.navigateTo({
      url: '../../me/pages/dolist/dolist'
    })
  },
  gomessage: function () {
    wx.navigateTo({
      url: '../../me/pages/message/message'
    })
  },
  /**
   * 跳转do卡页面
   */
  godocard:function(){
    wx.navigateTo({
      url: '../../me/pages/docard/docard'
    })
  },
  /**
   * 跳转添加参与者页面
   */
  goaddpartner:function(){
    wx.navigateTo({
      url: '../../me/pages/addpartner/addpartner'
    })
  },
  gosettings:function(){
    wx.navigateTo({
      url: '../../me/pages/settings/settings'
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    this.init();
  },

  gopartnerinfo:function(e){
    var pid = e.currentTarget.dataset.pid;
    console.log(pid);
    wx.navigateTo({
      url: '../../me/pages/partnerinfo/partnerinfo?pid='+pid
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  }
})