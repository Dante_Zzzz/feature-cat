import util from '../../../utils/util';
const _ = require('../../common/libs/lodash.core.min.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    show:false,
    channelType:0,
    readPicBookDisplayItemList: [],
    total:0,
    userRole:'',
    discoveryPageInfo:{
      pageNum:1,
      pageSize:10
    }
  },
  getUserInfoHandler: function (e) {
    var that = this;
    var userInfo = e.detail.userInfo;
    util.setUserInfoHandler(userInfo, function () {
      that.gocreatechannel();
    });
  },
  /*** 预览图片****/
  previewImage: function (e) {
    var current = e.currentTarget.dataset.src;
    wx.previewImage({
      current: current,// 当前显示图片的http链接 
      urls: this.data.bannerlist // 需要预览的图片http链接列表  
    })
  },
  preImage: function (e) {
    var current = e.currentTarget.dataset.src;
    var index = e.currentTarget.dataset.index;
    var urls = this.data.discoveryDoPlusList[index].doPlusShowBaseInfo.doPlusContent.imageContent
    wx.previewImage({
      current: current, // 当前显示图片的http链接
      urls: urls // 需要预览的图片http链接列表
    })
  },
  gochannelinfo: function (e) {
    var channelId = e.currentTarget.dataset.channelid;
    if(!channelId){
      return;
    }
    wx.navigateTo({
      url: '../../discover/pages/channelinfo/channelinfo?channelId=' + channelId
    })
  },
  likeDoPlus: function (e) {
    var that = this;
    var discoveryDoPlusList = that.data.discoveryDoPlusList;
    var islikeDoPlus = !e.currentTarget.dataset.ilike;
    var doPlusId = e.currentTarget.dataset.doplusid;
    var index = e.currentTarget.dataset.index;

    var likeDoPlusDTO = {
      doPlusId: doPlusId,
      islikeDoPlus: islikeDoPlus
    };
    util.wxRequest({
      url: 'likeDoPlus',
      data: { likeDoPlusDTO: likeDoPlusDTO },
      success: function (res) {
        discoveryDoPlusList[index].doPlusShowBaseInfo.ilike = islikeDoPlus;
        var likeCount = discoveryDoPlusList[index].doPlusShowBaseInfo.likeCount;
        if (islikeDoPlus) {
          likeCount++;
        } else {
          likeCount--;
        }
        discoveryDoPlusList[index].doPlusShowBaseInfo.likeCount = likeCount;
        that.setData({
          discoveryDoPlusList: discoveryDoPlusList
        });
      }
    })
  },
  //等比缩放图片并保存
  imageLoad: function (e) {
    //获取图片真实宽度  
    var imgwidth = e.detail.width,
      imgheight = e.detail.height,
      //宽高比  
      // ratio = imgwidth / imgheight;
      ratio = 690/260;
    //console.log(imgwidth, imgheight);
    //计算的高度值  

    var viewHeight = parseInt(this.data.scrollWidth) / ratio;
    var imgheight = viewHeight.toFixed(0);
    var imgheightarray = this.data.imgheights;
    //把每一张图片的高度记录到数组里
    imgheightarray.push(imgheight);

    this.setData({
      imgheights: imgheightarray,
    });
  },
  swiperChange: function (e) {

    this.setData({
      currentNavtab: e.detail.current
    })  
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        //获取屏幕的宽度并保存
        that.setData({
          scrollWidth: res.windowWidth
        });
      }
    });

    this.innerAudioContext = wx.createInnerAudioContext();
    this.innerAudioContext.onError((res) => {
      that.tip("播放录音失败！")
    })

    
    this.animation = wx.createAnimation({
      duration: 500,
      timingFunction: 'linear', // "linear","ease","ease-in","ease-in-out","ease-out","step-start","step-end"
      delay: 0,
      transformOrigin: '50% 50% 0',
      success: function (res) {
        console.log("res")
      }
    })
  },
  tip: function (msg) {
    wx.showModal({
      title: '提示',
      content: msg,
      showCancel: false
    })
  },
  stopAudio:function(){
    var that = this;
    that.innerAudioContext.pause();
    var discoveryDoPlusList = that.data.discoveryDoPlusList;
    for(var i =0;i<discoveryDoPlusList.length;i++){
      discoveryDoPlusList[i].doPlusShowBaseInfo.isPlay = false;
    }
    that.setData({
      discoveryDoPlusList: discoveryDoPlusList
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },
  getRandomRecommendChannelList:function(){
    var n = this.data.n;
    this.animation.rotate(180 * (n)).step()
    this.setData({
      animationData: this.animation.export()
    })
    var that = this;
    util.wxRequest({
      url:'getRandomRecommendChannelList',
      success:function(res){
        that.setData({
          n:n+3,
          recommendChannelList: res.recommendChannelList
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      userRole: wx.getStorageSync('userRole'),
      discoveryPageInfo: {
        pageNum: 1,
        pageSize: 10
      }
    });
    var token = wx.getStorageSync('token');
    if (token) {
      this.init();
    }
  },
  changeChannelType:function(){
    var channelType = this.data.channelType;
    var channelType = channelType === 1 ? 0:1;
    this.setData({
      channelType: channelType,
      discoveryPageInfo: {
        pageNum: 1,
        pageSize: 10
      }
    })
    this.init();
  },
  init:function(){
    var that = this;
    var isHideLoading = false;
    if (that.data.show) {
      isHideLoading = true;
    }
    var discoveryPageInfo = that.data.discoveryPageInfo;
    var channelType = that.data.channelType;
    util.wxRequest({
      url:'getDiscoveryPageInfoV2',
      data: { channelType: channelType,discoveryPageInfo: discoveryPageInfo},
      success:function(res){
        var discoveryPageInfo = res.discoveryPageInfo || {};
        discoveryPageInfo.pageNum++;


        var readPicBookDisplayItemList = res.readPicBookDisplayItemList;
        readPicBookDisplayItemList && readPicBookDisplayItemList.forEach(function(item){
          var tagList = item.picBookInfo.tagList;
          if (tagList && tagList.length){
            tagList.forEach(function(index){
              index.tagDilplay = index.tagDilplay.substr(0,5);
            })
          }
        })

        var allreadPicBookDisplayItemList = readPicBookDisplayItemList;
        if (discoveryPageInfo.pageNum > 2) {
          allreadPicBookDisplayItemList = that.data.readPicBookDisplayItemList.concat(readPicBookDisplayItemList);
        }

        that.setData({
          readPicBookDisplayItemList: allreadPicBookDisplayItemList,
          total: discoveryPageInfo.total,
          show:true
        })
      },
      onfail: function () {

      },
      isHideLoading: isHideLoading
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },
  getUserInfoByDolikeHandler:function(e){
    var that = this;
    var userInfo = e.detail.userInfo;
    util.setUserInfoHandler(userInfo, function () {
    });
  },
  getUserInfoByCommentHandler: function (e) {
    var that = this;
    var userInfo = e.detail.userInfo;
    util.setUserInfoHandler(userInfo, function () {
    });
  },
  gocreatechannel:function(){
    wx.navigateTo({
      url: '../../discover/pages/createchannel/createchannel'
    })
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var discoveryPageInfo = that.data.discoveryPageInfo;
    if (discoveryPageInfo && discoveryPageInfo.size >= discoveryPageInfo.pageNum) {
      that.init();
    }
  },
  gochannelinfobyswiper: function (e) {
    var itemId = e.currentTarget.dataset.itemid;
    var itemType = e.currentTarget.dataset.itemtype;
    if (itemType == 0){//频道
      wx.navigateTo({
        url: '../../discover/pages/channelinfo/channelinfo?channelId=' + itemId
      })
    }
    
  },
  gododetail: function (e) {
    var doPlusId = e.currentTarget.dataset.doplusid;
    var url = '../../home/pages/detail/detail?doPlusId=' + doPlusId;
    wx.navigateTo({
      url: url
    })
  },
  playVedio: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var discoveryDoPlusList = this.data.discoveryDoPlusList;
    for (var i = 0; i < discoveryDoPlusList.length;i++){
      discoveryDoPlusList[i].doPlusShowBaseInfo.showVedio = false;
    }
    discoveryDoPlusList[index].doPlusShowBaseInfo.showVedio = true;

    that.setData({
      discoveryDoPlusList: discoveryDoPlusList
    })
    that.stopAudio();
  },
  playAudio: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var discoveryDoPlusList = this.data.discoveryDoPlusList;
    var isPlay = discoveryDoPlusList[index].doPlusShowBaseInfo.isPlay;
    var duration = discoveryDoPlusList[index].doPlusShowBaseInfo.duration;

    var src = discoveryDoPlusList[index].doPlusShowBaseInfo.context;
    that.innerAudioContext.src = src;
    that.innerAudioContext.onPlay(() => {
      console.log('开始播放');
      discoveryDoPlusList[index].doPlusShowBaseInfo.isPlay = true;
      that.setData({
        discoveryDoPlusList: discoveryDoPlusList
      })
    })
    that.innerAudioContext.onPause(() => {
      console.log('暂停');
      discoveryDoPlusList[index].doPlusShowBaseInfo.isPlay = false;
      that.setData({
        discoveryDoPlusList: discoveryDoPlusList
      })
    })
    that.innerAudioContext.onStop(() => {
      console.log('停止')
      discoveryDoPlusList[index].doPlusShowBaseInfo.isPlay = false;
      that.setData({
        discoveryDoPlusList: discoveryDoPlusList
      })
    })
    that.innerAudioContext.onEnded(() => {
      console.log('结束')
      discoveryDoPlusList[index].doPlusShowBaseInfo.isPlay = false;
      discoveryDoPlusList[index].doPlusShowBaseInfo.progress = 100;
      that.setData({
        discoveryDoPlusList: discoveryDoPlusList
      })
    })


    if (isPlay) {
      discoveryDoPlusList[index].doPlusShowBaseInfo.isPlay = false;
      that.setData({
        discoveryDoPlusList: discoveryDoPlusList
      })
      that.innerAudioContext.pause();
    } else {
      that.innerAudioContext.play();
      that.innerAudioContext.onTimeUpdate(() => {
        var currentTime = that.innerAudioContext.currentTime;
        discoveryDoPlusList[index].doPlusShowBaseInfo.progress = currentTime * 1000 / duration * 100;
        discoveryDoPlusList[index].doPlusShowBaseInfo.nowduration = util.formarAudioTime(currentTime);
        that.setData({
          discoveryDoPlusList: discoveryDoPlusList
        })
      })
    }
  },
  showall: function (e) {
    var index = e.currentTarget.dataset.index;
    var discoveryDoPlusList = this.data.discoveryDoPlusList;
    discoveryDoPlusList[index].doPlusShowBaseInfo.showall = false;
    discoveryDoPlusList[index].doPlusShowBaseInfo.textContentPreview = discoveryDoPlusList[index].doPlusShowBaseInfo.doPlusContent.textContent;
    this.setData({
      discoveryDoPlusList: discoveryDoPlusList
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})