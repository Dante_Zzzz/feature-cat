import util from '../../../../utils/util';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    channelDefaultImgList:[],
    saveclass:'unsave',
    channelName:'',
    channelImgUrl:'',
    channelDescription:'',
    creatorDescription: '',
    channelNameLen: 0,
    channelDescriptionLen: 0,
    creatorDescriptionLen: 0,
    channelNameClass: '',
    channelDescriptionClass: '',
    creatorDescriptionClass: ''
  },
  save:function(){
    if (this.data.saveclass == 'unsave'){
      return;
    }
    this.saveChannelInfo();
    wx.navigateTo({
      url: 'savechannel',
    })
  },
  cancel:function(){
    wx.navigateBack()
  },
  bindInputChannelName: function (e) {
    var channelName = e.detail.value;
    this.setData({
      channelName: channelName.trim(),
      channelNameLen: channelName.trim().length
    })
    this.checkSaveClass();
  },
  bindInputChannelDescription: function (e) {
    var channelDescription = e.detail.value;
    this.setData({
      channelDescription: channelDescription.trim(),
      channelDescriptionLen: channelDescription.trim().length
    })
    this.checkSaveClass();
  },
  bindInputCreatorDescription: function (e) {
    var creatorDescription = e.detail.value;
    this.setData({
      creatorDescription: creatorDescription.trim(),
      creatorDescriptionLen: creatorDescription.trim().length
    })
    this.checkSaveClass();
  },
  checkSaveClass:function(){
    var data = this.data;
    var saveclass = 'unsave'
    if (data.channelName && data.channelImgUrl) {
      saveclass = 'save';
    }
    this.setData({
      saveclass: saveclass
    })
  },
  chooseImg:function(){
    var that = this;
    wx.showActionSheet({
      itemList: ['从默认图片选择', '从手机相册选择'],
      itemColor:'#333',
      success: function (res) {
        var index = res.tapIndex;
        if(index == 0){
          var imgs = JSON.stringify(that.data.channelDefaultImgList);
          wx.navigateTo({
            url:'channelimg?imgs='+imgs
          })
        }else{
          that.didPressChooseImage();
        }
      },
      fail: function (res) {
        console.log(res.errMsg)
      }
    })

  },
  didPressChooseImage: function () {
    var that = this;
    // 选择图片
    wx.chooseImage({
      count: 1,
      success: function (res) {
        var src = res.tempFilePaths[0];
          wx.showLoading();

          util.upload(src, function(res){
            that.setData({
              'channelImgUrl': res.imageURL,
            });
            that.checkSaveClass();
            wx.hideLoading();
          }, function(){
            wx.hideLoading();
          });


      }



    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.init();
  },
  init: function () {
    var that = this;
    util.wxRequest({
      url: 'getCreateChannelPageInfo',
      success: function (res) {
        var channelDefaultImgList = res.channelDefaultImgList;
        that.setData({
          channelDefaultImgList: channelDefaultImgList
        })
        
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },
  saveChannelInfo:function(){
    var data = this.data;
    wx.setStorageSync(
      'channelInfo',
      {
        channelName: data.channelName,
        channelImgUrl: data.channelImgUrl,
        channelDescription: data.channelDescription,
        creatorDescription: data.creatorDescription
      }
    )
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  }
})