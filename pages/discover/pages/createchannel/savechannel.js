import util from '../../../../utils/util'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    saveclass:'unsave',
    participantList: [],
    notifySwitch: false,
    notifyTime: '20:00',
    participantId: '',
    channelType: 0
  },
  
  changeNotifySwitch:function(e){
    var notifyswitch = e.currentTarget.dataset.notifyswitch;
    var value = true;
    if (notifyswitch == "false"){
      value = false;
    }
    this.setData({
      notifySwitch: value
    })
  },
  changeChannelType:function(e){
    var channeltype = e.currentTarget.dataset.channeltype;
    this.setData({
      channelType: +channeltype
    })
  },
  checkSaveClass:function(){
    var data = this.data;
    var saveclass = 'unsave'
    if (data.participantId) {
      saveclass = 'save';
    }
    this.setData({
      saveclass: saveclass
    })
  },
  refreshPrevPage: function () {
    var pages = getCurrentPages();
    if (pages.length > 1) {
      var prePage = pages[pages.length - 3];
      prePage.init();
    }
  },
  save:function(){
    if (this.data.saveclass == 'unsave'){
      return;
    }
    var that = this;
    var data = this.data;
    var channelInfo = wx.getStorageSync('channelInfo');
    var createChannelInfo = {
      channelName: channelInfo.channelName,
      channelImgUrl:channelInfo.channelImgUrl,
      channelDescription: channelInfo.channelDescription,
      creatorDescription: channelInfo.creatorDescription,
      notifySwitch: data.notifySwitch,
      notifyTime: data.notifyTime.replace(/:/g,'-'),
      participantId: data.participantId,
      channelType: data.channelType
    };
    util.wxRequest({
      url:'createChannel',
      data: { channelBaseInfoDTO: createChannelInfo},
      success:function(res){
        wx.showToast({
          title: '创建成功',
          duration:2000
        })
        that.refreshPrevPage();
        wx.navigateBack({
          delta:2
        })
      }
    })
  },
  init:function(flag){
    var that = this;
    var tmp = that.data.participantList;
    var tmpstatus = [];
    if(tmp){
      for(var i in tmp){
        var status = tmp[i].status;
        tmpstatus.push(status);
      } 
    }
    util.wxRequest({
      url: 'getCreateChannelPageInfo',
      success: function (res) {
        var participantList = res.participantList;
        if (participantList){
          for (var i in participantList){
            if (tmpstatus && tmpstatus[i] === 1){
              participantList[i].statusimg = '../../../../images/selpartner.png';
              participantList[i].status = 1;
            }else{
              participantList[i].statusimg = '../../../../images/unselpartner.png';
              participantList[i].status = 0;
            }
          }
          that.setData({
            participantList: participantList
          })
        }
      }
    })
  },
  bindTimeChange:function(e){
    this.setData({
      notifyTime: e.detail.value
    })
  },
  changestatus:function(e){
    var index = e.currentTarget.dataset.index;
    var status = e.currentTarget.dataset.status;
    var participantList = this.data.participantList;
    var participantId;
    
    if (participantList.length > 1) {
      for (var i in participantList) {
        participantList[i].status = 0;
        participantList[i].statusimg = '../../../../images/unselpartner.png';
      }

    }
    var num = 0;
    var img = '../../../../images/unselpartner.png';
    if(status == 0){
      num = 1;
      img = '../../../../images/selpartner.png';
      participantId = participantList[index].pid;
    }
    participantList[index].status = num;
    participantList[index].statusimg = img;
    this.setData({
      participantList: participantList,
      participantId: participantId
    })
    this.checkSaveClass();
  },
  goaddpartner:function(){
    wx.navigateTo({
      url: '../../../me/pages/addpartner/addpartner',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.init(true);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  }
})