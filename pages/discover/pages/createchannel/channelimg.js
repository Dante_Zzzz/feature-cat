// pages/discover/pages/createchannel/channelimg.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    channelDefaultImgList:[]
  },
  changestatus:function(e){
    var index = e.currentTarget.dataset.index;
    var imgs = this.data.channelDefaultImgList;
    for (var i = 0; i < imgs.length; i++) {
      imgs[i].status = false;
    }
    imgs[index].status = true;
    this.setData({
      channelDefaultImgList: imgs
    })   
    let pages = getCurrentPages();
    let prevPage = pages[pages.length - 2];
    prevPage.setData({
      channelImgUrl: imgs[index].img
    });
    setTimeout(function(){
      wx.navigateBack();
    },400)
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var imgs = JSON.parse(options.imgs);
    var arr = [];
    for(var i=0;i<imgs.length;i++){
      var img = imgs[i];
      arr.push({status:false,img:img})
    }
    this.setData({
      channelDefaultImgList: arr
    })   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  }
})