import util from '../../../../utils/util';
const Promise = require('../../../common/libs/es6-promise');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    isChannelOwner:false,
    saveclass:'save',
    channelDescription: "",
    channelId:"",
    channelImgUrl :"",
    channelName:"",
    channelType:0,
    creatorDescription:"",
    notifySwitch:false,
    notifyTime:"20:00",
    participantId:  "",
    channelNameLen:0,
    channelDescriptionLen:0,
    creatorDescriptionLen:0,
    channelDefaultImgList:[]
  },
  goaddpartner: function () {
    wx.navigateTo({
      url: '../../../me/pages/addpartner/addpartner',
    })
  },
  bindTimeChange: function (e) {
    this.setData({
      notifyTime: e.detail.value
    })
  },
  bindInputChannelName: function (e) {
    var channelName = e.detail.value;
    this.setData({
      channelName: channelName.trim(),
      channelNameLen: channelName.trim().length
    })
    console.log(channelName)
    this.checkSaveClass();
  },
  bindInputChannelDescription: function (e) {
    var channelDescription = e.detail.value;
    this.setData({
      channelDescription: channelDescription.trim(),
      channelDescriptionLen: channelDescription.trim().length
    })
    this.checkSaveClass();
  },
  bindInputCreatorDescription: function (e) {
    var creatorDescription = e.detail.value;
    this.setData({
      creatorDescription: creatorDescription.trim(),
      creatorDescriptionLen: creatorDescription.trim().length
    })
    this.checkSaveClass();
  },
  checkSaveClass: function () {
    var data = this.data;
    var saveclass = 'unsave'
    if (data.channelName && data.channelImgUrl && data.participantId) {
      saveclass = 'save';
    }
    this.setData({
      saveclass: saveclass
    })
  },
  switchChange:function(e){
    this.setData({
      notifySwitch:e.detail.value
    })
  },
  getCreateChannelPageInfo:function(){
    const deferred = util.getDeferred();
    var that = this;
    var tmp = that.data.participantList;
    var tmpstatus = [];
    if (tmp) {
      for (var i in tmp) {
        var status = tmp[i].status;
        tmpstatus.push(status);
      }
    }
    util.wxRequest({
      url: 'getCreateChannelPageInfo',
      success: function (res) {
        var participantList = res.participantList;
        if (participantList) {
          for (var i in participantList) {
            if (tmpstatus && tmpstatus[i] === 1) {
              participantList[i].statusimg = '../../../../images/selpartner.png';
              participantList[i].status = 1;
            } else {
              participantList[i].statusimg = '../../../../images/unselpartner.png';
              participantList[i].status = 0;
            }
          }
          that.setData({
            channelDefaultImgList: res.channelDefaultImgList
          })
          deferred.resolve(participantList);

        }
      }
    })

    return deferred.promise;
  },
  quitChannel:function(){
    var that = this;
    var channelId = this.data.channelId;
    wx.showModal({
      title: '',
      content: '确定要退出频道吗',
      success: function (res) {
        if (res.confirm) {

          util.wxRequest({
            url: 'quitChannel',
            data: { channelId: channelId },
            success: function (res) {
              that.refreshPrevPage();
              wx.navigateBack();
            }
          });
        } else if (res.cancel) {

        }
      }
    })


  },
  init:function(){
    var that = this;
    Promise.all([that.getChannelInfo(), that.getCreateChannelPageInfo()]).then(function (data) {
      var channelInfo = data[0];
      var participantList = data[1];
      var participantId = channelInfo.participantId;
      for (var i in participantList) {
        participantList[i].status = 0;
        if (participantList[i].pid == participantId) {
          participantList[i].status = 1;
          participantList[i].statusimg = '../../../../images/selpartner.png';
        }
      }
      var channelDescriptionLen = 0;
      var creatorDescriptionLen = 0;
      if (channelInfo.channelDescription && channelInfo.channelDescription.length) {
        channelDescriptionLen = channelInfo.channelDescription.length;
      }
      if (channelInfo.creatorDescription && channelInfo.creatorDescription.length) {
        creatorDescriptionLen = channelInfo.creatorDescription.length;
      }
      that.setData({
        channelDescription: channelInfo.channelDescription,
        channelImgUrl: channelInfo.channelImgUrl,
        channelName: channelInfo.channelName,
        channelType: channelInfo.channelType,
        creatorDescription: channelInfo.creatorDescription,
        notifySwitch: channelInfo.notifySwitch,
        notifyTime: channelInfo.notifyTime.replace(/-/g, ':'),
        participantId: participantId,
        channelNameLen: channelInfo.channelName.length,
        channelDescriptionLen: channelDescriptionLen,
        creatorDescriptionLen: creatorDescriptionLen,
        participantList: participantList
      })

    })
  },
  getChannelInfo: function (){
    var channelId = this.data.channelId;
    const deferred = util.getDeferred();
    var that = this;
    util.wxRequest({
      url:'getChannelInfo',
      data: { channelId: channelId},
      success:function(res){
        deferred.resolve(res.channelInfo);
      }
    })

    return deferred.promise;
  },
  changestatus: function (e) {
    var index = e.currentTarget.dataset.index;
    var status = e.currentTarget.dataset.status;
    var participantList = this.data.participantList;
    var participantId;

    if (participantList.length > 1) {
      for (var i in participantList) {
        participantList[i].status = 0;
        participantList[i].statusimg = '../../../../images/unselpartner.png';
      }

    }
    var num = 0;
    var img = '../../../../images/unselpartner.png';
    if (status == 0) {
      num = 1;
      img = '../../../../images/selpartner.png';
      participantId = participantList[index].pid;
    }
    participantList[index].status = num;
    participantList[index].statusimg = img;
    this.setData({
      participantList: participantList,
      participantId: participantId
    })
    this.checkSaveClass();
  },
  didPressChooseImage: function () {
    var that = this;
    // 选择图片
    wx.chooseImage({
      count: 1,
      success: function (res) {
        var src = res.tempFilePaths[0];
        wx.showLoading();
        util.upload(src, function(res){
          that.setData({
            'channelImgUrl': res.imageURL,
          });
          that.checkSaveClass();
          wx.hideLoading();
        }, function(){
          wx.hideLoading();
        });

       
      }



    })
  },
  refreshPrevPage: function () {
    var pages = getCurrentPages();
    if (pages.length > 1) {
      var prePage = pages[pages.length - 2];
      prePage.init();
    }
  },
  save: function () {
    if (this.data.saveclass == 'unsave') {
      return;
    }
    var that = this;
    var data = this.data;
    var channelInfo = {
      channelId: data.channelId,
      channelName: data.channelName,
      channelImgUrl: data.channelImgUrl,
      channelDescription: data.channelDescription,
      creatorDescription: data.creatorDescription,
      notifySwitch: data.notifySwitch,
      notifyTime: data.notifyTime.replace(/:/g, '-'),
      participantId: data.participantId,
      channelType: data.channelType
    };
    util.wxRequest({
      url: 'modifyChannelInfo',
      data: { channelInfo: channelInfo },
      success: function (res) {
        that.refreshPrevPage();
        wx.navigateBack();
      }
    })
  },
  changeChannelType: function (e) {
    var channeltype = e.currentTarget.dataset.channeltype;
    this.setData({
      channelType: +channeltype
    })
  },
  chooseImg: function () {
    var that = this;
    wx.showActionSheet({
      itemList: ['从默认图片选择', '从手机相册选择'],
      itemColor: '#333',
      success: function (res) {
        var index = res.tapIndex;
        if (index == 0) {
          var imgs = JSON.stringify(that.data.channelDefaultImgList);
          wx.navigateTo({
            url: '../createchannel/channelimg?imgs=' + imgs
          })
        } else {
          that.didPressChooseImage();
        }
      },
      fail: function (res) {
        console.log(res.errMsg)
      }
    })

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.setData({
      channelId: options.channelId,
      isChannelOwner:options.isChannelOwner
    });
    
    this.init();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  }
})