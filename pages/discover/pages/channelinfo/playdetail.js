import util from '../../../../utils/util';
var WxParse = require('../../../../wxParse/wxParse.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    show:false,
    tab:1,
    tabclass:'',
    inChannel: false,
    channelId:'',
    discoveryDoPlusList:[],
    onlineChannelInfoDTO:{},
    topicDetail: {},
    topidId:'',
    from:'',
    pageInfo: {
      pageNum: 1,
      pageSize: 10
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // wx.showShareMenu({
    //   withShareTicket: true
    // });
    var that = this;
    var topicId = options.topicId;
    var channelId = options.channelId;
    this.setData({
      topicId: topicId,
      channelId: channelId,
      from: options.from || ''
    })
    
    this.innerAudioContext = wx.createInnerAudioContext();
    this.innerAudioContext.onError((res) => {
      that.tip("播放录音失败！")
    })
  },
  gohome: function () {
    wx.switchTab({
      url: '/pages/tabBar/discover/index'
    })
  },
  scrollToContent: function () {
    var that = this;
    that.setData({
      tab: 1
    })
    wx.createSelectorQuery().select('#js_content').boundingClientRect().selectViewport().scrollOffset().exec(function (res) {
      wx.pageScrollTo({
        scrollTop: res[0].top + res[1].scrollTop-50,
        duration: 300
      })
    })
  },
  scrollToPractice: function () {
    var that = this;
    that.setData({
      tab: 2
    })
    wx.createSelectorQuery().select('#js_practice').boundingClientRect().selectViewport().scrollOffset().exec(function (res) {
      wx.pageScrollTo({
        scrollTop: res[0].top + res[1].scrollTop-50,
        duration: 300
      })
    })
  },
  scrollToDoplus: function () {
    var that = this;
    that.setData({
      tab: 3
    })
    wx.createSelectorQuery().select('#js_doplus').boundingClientRect().selectViewport().scrollOffset().exec(function (res) {
      wx.pageScrollTo({
        scrollTop: res[0].top + res[1].scrollTop-50,
        duration: 300
      })
    })
  },
  getUserInfoHandler: function (e) {
    var that = this;
    var userInfo = e.detail.userInfo;
    util.setUserInfoHandler(userInfo, function () {
      that.joinChannel(e);
    });
  },
  getUserInfoByDoPlusHandler: function (e) {
    var that = this;
    
    var userInfo = e.detail.userInfo;
    util.setUserInfoHandler(userInfo, function () {
      that.doplus();
    });
  },
  getUserInfoByDolikeHandler: function (e) {
    var that = this;
    var islikeDoPlus = !e.currentTarget.dataset.ilike;
    var doPlusId = e.currentTarget.dataset.doplusid;
    var index = e.currentTarget.dataset.index;

    var userInfo = e.detail.userInfo;
    util.setUserInfoHandler(userInfo, function () {
      that.likeDoPlus(islikeDoPlus, doPlusId, index);
    });
  },
  getUserInfoByCommentHandler: function (e) {
    var that = this;
    var userInfo = e.detail.userInfo;
    var doPlusId = e.currentTarget.dataset.doplusid;
    util.setUserInfoHandler(userInfo, function () {
      that.gododetail(doPlusId);
    });
  },
  gododetail: function (doPlusId) {
    // var doPlusId = e.currentTarget.dataset.doplusid;
    var url = '../../../home/pages/detail/detail?doPlusId=' + doPlusId + '&from=topic';
    wx.navigateTo({
      url: url
    })
  },
  gododetailInfo: function (e) {
    var doPlusId = e.currentTarget.dataset.doplusid;
    var url = '../../../home/pages/detail/detail?doPlusId=' + doPlusId + '&from=topic';
    wx.navigateTo({
      url: url
    })
  },
  gochannel: function () {
    var channelId = this.data.onlineChannelInfoDTO.channelId;
    var url = 'channelinfo?channelId=' + channelId;
    wx.navigateTo({
      url: url
    })
  },
  joinChannel: function () {
    var that = this;
    var channelId = this.data.channelId;
    util.wxRequest({
      url: 'joinChannel',
      data: { channelId, channelId },
      success: function (res) {
        that.setData({
          inChannel: true,
        })

      }
    })
  },
  godolpus:function(){
    var topicId = this.data.topicId;
    var channelId = this.data.channelId;
    var url = '../../../home/pages/do/do?channelId=' + channelId;
    if (topicId) {
      url += '&topidId=' + topicId;
    }
    wx.navigateTo({
      url: url
    })
  },
  tip: function (msg) {
    wx.showModal({
      title: '提示',
      content: msg,
      showCancel: false
    })
  },
  stopAudio:function(){
    // var that = this;
    // that.innerAudioContext.pause();
    // var contentList = that.data.contentList;
    // for(var i =0;i<contentList.length;i++){
    //   contentList[i].isPlay = false;
    // }
    // that.setData({
    //   contentList: contentList
    // })
  },
  playVideoContent: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var topicDetail = that.data.topicDetail;
    var contentList = that.data.topicDetail.content.contentList;
    contentList[index].showVedio = true;
    topicDetail.content.contentList = contentList;
    that.setData({
      topicDetail: topicDetail
    })
    that.stopAudio();

  },
  playVideoCommonKnowledgeContent: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var topicDetail = that.data.topicDetail;
    var contentList = that.data.topicDetail.commonKnowledgeContent.contentList;
    contentList[index].showVedio = true;
    topicDetail.commonKnowledgeContent.contentList = contentList;
    that.setData({
      topicDetail: topicDetail
    })
    that.stopAudio();

  },
  playVideoPracticeContent: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var topicDetail = that.data.topicDetail;
    var contentList = that.data.topicDetail.practiceContent.contentList;
    contentList[index].showVedio = true;
    topicDetail.practiceContent.contentList = contentList;
    that.setData({
      topicDetail: topicDetail
    })
    that.stopAudio();

  },
  
  playVideo: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var discoveryDoPlusList = this.data.discoveryDoPlusList;
    for (var i = 0; i < discoveryDoPlusList.length; i++) {
      discoveryDoPlusList[i].showVedio = false;
    }
    discoveryDoPlusList[index].showVedio = true;
    that.setData({
      discoveryDoPlusList: discoveryDoPlusList
    })
    that.stopAudio();
  },
  startPlayAudio: function (src, type) {
    var that = this;
    var audioPlayer = wx.getBackgroundAudioManager();
    var contentList = type === 'list' ? this.data.discoveryDoPlusList : this.data.topicDetail.content.contentList;
    var renewData = function (contents) {
      if (type === 'list') {
        that.setData({
          discoveryDoPlusList: contents
        })
      } else {
        var topicDetail = that.data.topicDetail;
        topicDetail.content.contentList = contents;
        that.setData({
          topicDetail: topicDetail
        })
      }
    }

    if (audioPlayer.duration && !audioPlayer.paused) {
      audioPlayer.pause();

      if (audioPlayer.src === src) {
        return;
      } else {
        contentList.forEach(list => {
          if (Object.keys(list).indexOf('isPlay') !== -1) {
            list.isPlay = false;
            list.progress = 0;
            list.nowduration = 0
            if (list.doPlusShowBaseInfo) list.doPlusShowBaseInfo.nowduration = 0;
          }
        })
        renewData(contentList);
      }
    } else if (audioPlayer.duration && audioPlayer.paused) {
      if (audioPlayer.src === src && audioPlayer.currentTime >= audioPlayer.duration) {
        audioPlayer.src = src + `?_t=${ +new Date() }`
        return;
      } else if (audioPlayer.src === src) {
        audioPlayer.play();
        return;
      } else {
        contentList.forEach(list => {
          if (Object.keys(list).indexOf('isPlay') !== -1) {
            list.isPlay = false;
            list.progress = 0;
            list.nowduration = 0
            if (list.doPlusShowBaseInfo) list.doPlusShowBaseInfo.nowduration = 0;
          }
        })
        renewData(contentList);
      }
    }
    audioPlayer.src = src;
    audioPlayer.title = '卡乐猫喵喵乐园';
    audioPlayer.play()
  },
  playAudio: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var contentList = this.data.discoveryDoPlusList;
    var isPlay = contentList[index].isPlay;
    var duration = contentList[index].doPlusShowBaseInfo.duration;
    var audioPlayer = wx.getBackgroundAudioManager()

    var src = contentList[index].doPlusShowBaseInfo.doPlusContent.soundContent.soundUrl;
    that.startPlayAudio(src, 'list')
    audioPlayer.onPlay(() => {
      console.log('开始播放');
      contentList[index].isPlay = true;
      that.setData({
        discoveryDoPlusList: contentList
      })
    })
    audioPlayer.onPause(() => {
      console.log('暂停');
      contentList[index].isPlay = false;
      that.setData({
        discoveryDoPlusList: contentList
      })
    })
    audioPlayer.onStop(() => {
      console.log('停止')
      contentList[index].isPlay = false;
      that.setData({
        discoveryDoPlusList: contentList
      })
    })
    audioPlayer.onEnded(() => {
      console.log('结束')
      contentList[index].isPlay = false;
      contentList[index].progress = 0;
      contentList[index].doPlusShowBaseInfo.nowduration = 0
      that.setData({
        discoveryDoPlusList: contentList
      })
    })


    if (isPlay) {
      contentList[index].isPlay = false;
      that.setData({
        discoveryDoPlusList: contentList
      })
      audioPlayer.pause();
    } else {
      audioPlayer.play();
      audioPlayer.onTimeUpdate(() => {
        var currentTime = audioPlayer.currentTime;
        contentList[index].progress = currentTime * 1000 / duration * 100;
        contentList[index].doPlusShowBaseInfo.nowduration = util.formarAudioTime(currentTime);
        that.setData({
          discoveryDoPlusList: contentList
        })
      })
    }
  },
  playAudioContent: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var contentList = that.data.topicDetail.content.contentList;
    var topicDetail = that.data.topicDetail;
    var isPlay = contentList[index].isPlay;
    var duration = contentList[index].duration;
    var audioPlayer = wx.getBackgroundAudioManager()

    var src = contentList[index].url;
    that.startPlayAudio(src, 'content');
    audioPlayer.onPlay(() => {
      console.log('开始播放');
      contentList[index].isPlay = true;
      topicDetail.content.contentList = contentList;
      that.setData({
        topicDetail: topicDetail
      })
    })
    audioPlayer.onPause(() => {
      console.log('暂停');
      contentList[index].isPlay = false;
      topicDetail.content.contentList = contentList;
      that.setData({
        topicDetail: topicDetail
      })
    })
    audioPlayer.onStop(() => {
      console.log('停止')
      contentList[index].isPlay = false;
      topicDetail.content.contentList = contentList;
      that.setData({
        topicDetail: topicDetail
      })
    })
    audioPlayer.onEnded(() => {
      console.log('结束')
      contentList[index].isPlay = false;
      contentList[index].progress = 0;
      contentList[index].nowduration = 0
      topicDetail.content.contentList = contentList;
      that.setData({
        topicDetail: topicDetail
      })
    })


    if (isPlay) {
      contentList[index].isPlay = false;
      topicDetail.content.contentList = contentList;
      that.setData({
        topicDetail: topicDetail
      })
      audioPlayer.pause();
    } else {
      audioPlayer.play();
      audioPlayer.onTimeUpdate(() => {
        var currentTime = audioPlayer.currentTime;
        contentList[index].progress = currentTime * 1000 / duration * 100;
        contentList[index].nowduration = util.formarAudioTime(currentTime);
        topicDetail.content.contentList = contentList;
        that.setData({
          topicDetail: topicDetail
        })
      })
    }
  },
  changeAudioProgress: function (e) {
    var that = this;
    var value = e.detail.value;
    var index = e.currentTarget.dataset.index;
    var type = e.currentTarget.dataset.type;
    var contentList = type === 'list' ? that.data.discoveryDoPlusList : that.data.topicDetail.content.contentList;
    var renewData = function (contents) {
      if (type === 'list') {
        that.setData({
          discoveryDoPlusList: contents
        })
      } else {
        var topicDetail = that.data.topicDetail;
        topicDetail.content.contentList = contents;
        that.setData({
          topicDetail: topicDetail
        })
      }
    }

    var audioPlayer = wx.getBackgroundAudioManager();
    var duration = audioPlayer.duration;

    if (!duration) {
      var src = contentList[index].url;
      that.startPlayAudio(src, type)
      duration = audioPlayer.duration;
    }

    var targetTime = Math.floor(duration * (value / 100));

    if (audioPlayer.paused) {
      audioPlayer.play()
    }
    wx.seekBackgroundAudio({
      position: targetTime,
      success: function() {
        var currentTime = audioPlayer.currentTime;
        contentList[index].progress = currentTime * 1000 / duration * 100;
        renewData(contentList)
      }
    })
  },
  playAudioPracticeContent: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var contentList = that.data.topicDetail.practiceContent.contentList;
    var topicDetail = that.data.topicDetail;
    var isPlay = contentList[index].isPlay;
    var duration = contentList[index].duration;

    var src = contentList[index].url;
    that.innerAudioContext.src = src;
    that.innerAudioContext.onPlay(() => {
      console.log('开始播放');
      contentList[index].isPlay = true;
      topicDetail.practiceContent.contentList = contentList;
      that.setData({
        topicDetail: topicDetail
      })
    })
    that.innerAudioContext.onPause(() => {
      console.log('暂停');
      contentList[index].isPlay = false;
      topicDetail.practiceContent.contentList = contentList;
      that.setData({
        topicDetail: topicDetail
      })
    })
    that.innerAudioContext.onStop(() => {
      console.log('停止')
      contentList[index].isPlay = false;
      topicDetail.practiceContent.contentList = contentList;
      that.setData({
        topicDetail: topicDetail
      })
    })
    that.innerAudioContext.onEnded(() => {
      console.log('结束')
      contentList[index].isPlay = false;
      contentList[index].progress = 100;
      topicDetail.practiceContent.contentList = contentList;
      that.setData({
        topicDetail: topicDetail
      })
    })


    if (isPlay) {
      contentList[index].isPlay = false;
      topicDetail.practiceContent.contentList = contentList;
      that.setData({
        topicDetail: topicDetail
      })
      that.innerAudioContext.pause();
    } else {
      that.innerAudioContext.play();
      that.innerAudioContext.onTimeUpdate(() => {
        var currentTime = that.innerAudioContext.currentTime;
        contentList[index].progress = currentTime * 1000 / duration * 100;
        contentList[index].nowduration = util.formarAudioTime(currentTime);
        topicDetail.practiceContent.contentList = contentList;
        that.setData({
          topicDetail: topicDetail
        })
      })
    }
  },
  playAudioCommonKnowledgeContent: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var contentList = that.data.topicDetail.commonKnowledgeContent.contentList;
    var topicDetail = that.data.topicDetail;
    var isPlay = contentList[index].isPlay;
    var duration = contentList[index].duration;

    var src = contentList[index].url;
    that.innerAudioContext.src = src;
    that.innerAudioContext.onPlay(() => {
      console.log('开始播放');
      contentList[index].isPlay = true;
      topicDetail.commonKnowledgeContent.contentList = contentList;
      that.setData({
        topicDetail: topicDetail
      })
    })
    that.innerAudioContext.onPause(() => {
      console.log('暂停');
      contentList[index].isPlay = false;
      topicDetail.commonKnowledgeContent.contentList = contentList;
      that.setData({
        topicDetail: topicDetail
      })
    })
    that.innerAudioContext.onStop(() => {
      console.log('停止')
      contentList[index].isPlay = false;
      topicDetail.commonKnowledgeContent.contentList = contentList;
      that.setData({
        topicDetail: topicDetail
      })
    })
    that.innerAudioContext.onEnded(() => {
      console.log('结束')
      contentList[index].isPlay = false;
      contentList[index].progress = 100;
      topicDetail.commonKnowledgeContent.contentList = contentList;
      that.setData({
        topicDetail: topicDetail
      })
    })


    if (isPlay) {
      contentList[index].isPlay = false;
      topicDetail.commonKnowledgeContent.contentList = contentList;
      that.setData({
        topicDetail: topicDetail
      })
      that.innerAudioContext.pause();
    } else {
      that.innerAudioContext.play();
      that.innerAudioContext.onTimeUpdate(() => {
        var currentTime = that.innerAudioContext.currentTime;
        contentList[index].progress = currentTime * 1000 / duration * 100;
        contentList[index].nowduration = util.formarAudioTime(currentTime);
        topicDetail.commonKnowledgeContent.contentList = contentList;
        that.setData({
          topicDetail: topicDetail
        })
      })
    }
  },
  doplus: function () {
    var topicDetail = this.data.topicDetail;
    var topicId = topicDetail.topicId;
    var topicTitle = topicDetail.topicTitle;
    var topicOrder = topicDetail.topicOrder;
    var channelId = this.data.channelId;
    var url = '../../../home/pages/do/do?channelId=' + channelId;
    url += '&topicId=' + topicId + '&topicTitle=' + topicTitle + '&topicOrder=' + topicOrder;
    wx.navigateTo({
      url: url
    })

  },
  preImage: function (e) {
    this.handleOnShow = true;
    var current = e.currentTarget.dataset.src;
    var index = e.currentTarget.dataset.index;
    var urls = this.data.discoveryDoPlusList[index].doPlusShowBaseInfo.doPlusContent.imageContent;
    wx.previewImage({
      current: current, // 当前显示图片的http链接
      urls: urls // 需要预览的图片http链接列表
    })
  },
  showall: function (e) {
    var index = e.currentTarget.dataset.index;
    var discoveryDoPlusList = this.data.discoveryDoPlusList;
    discoveryDoPlusList[index].doPlusShowBaseInfo.showall = false;
    discoveryDoPlusList[index].doPlusShowBaseInfo.textContentPreview = discoveryDoPlusList[index].doPlusShowBaseInfo.doPlusContent.textContent;
    this.setData({
      discoveryDoPlusList: discoveryDoPlusList
    })
  },
  init: function (){
    var that = this;
    var topicId = this.data.topicId;
    var pageInfo = that.data.pageInfo;
    util.wxRequest({
      url:'getOnlineTopicDetail',
      data: { topicId: topicId, pageInfo: pageInfo},
      success:function(res){
        var pageInfo = res.pageInfoDTO || {};
        pageInfo.pageNum++;
        var discoveryDoPlusList = res.discoveryDoPlusList || [];
        var discoveryDoPlusList = res.discoveryDoPlusList || [];
        if (discoveryDoPlusList && discoveryDoPlusList.length) {
          for (var i = 0; i < discoveryDoPlusList.length; i++) {
            var item = discoveryDoPlusList[i].doPlusShowBaseInfo;
            var textContentPreview = item.textContentPreview;
            var textContent = item.doPlusContent.textContent;
            if (textContentPreview && textContent && textContentPreview != textContent) {
              item.showall = true;
            }
            if (item.doPlusContent.soundContent) {
              var thumb = item.doPlusContent.soundContent.soundSeconds / 1000;
              var formatduration = util.formarAudioTime(thumb);
              item.nowduration = '0"';
              item.duration = thumb * 1000;
              item.formatduration = formatduration;
              item.context = item.doPlusContent.soundContent.soundUrl;
            }

            if (item.doPlusContent.videoContent) {
              var videoWidth = item.doPlusContent.videoContent.videoWidth;
              var videoHeight = item.doPlusContent.videoContent.videoHeight;
              var videoSize = util.getVideoSize(videoWidth, videoHeight);
              videoWidth = videoSize.videoWidth;
              videoHeight = videoSize.videoHeight;
              item.doPlusContent.videoContent.videoWidth = videoWidth;
              item.doPlusContent.videoContent.videoHeight = videoHeight;

            }

          }
        }


        var alldiscoveryDoPlusList = discoveryDoPlusList;
        if (pageInfo.pageNum > 2) {
          alldiscoveryDoPlusList = that.data.discoveryDoPlusList.concat(discoveryDoPlusList);
        }

        var topicDetail = res.topicDetail;
        if (topicDetail && topicDetail.content && topicDetail.content.contentList && topicDetail.content.contentList.length) {
          var contentList = topicDetail.content.contentList;
          for (var i = 0; i < contentList.length; i++) {
            var item = contentList[i];
            if (item.contentType == 2) {
              var thumb = item.soundLength;
              var formatduration = util.formarAudioTime(thumb);
              item.nowduration = '0"';
              item.duration = thumb * 1000;
              item.formatduration = formatduration;
              item.context = item.context;
            }
            if (item.contentType == 3) {
              var videoWidth = item.width;
              var videoHeight = item.height;
              if (videoWidth > videoHeight) {
                videoHeight = videoHeight / 3;
                videoWidth = videoWidth / 3;
              } else {
                videoHeight = videoHeight / 3.5;
                videoWidth = videoWidth / 3.5;
              }
              if (videoWidth > 240) {
                videoHeight = videoHeight / (videoWidth / 240);
                videoWidth = 240;
              }
              item.videoWidth = videoWidth;
              item.videoHeight = videoHeight;


            }
            if (item.contentType === 4) {
              var richContent = contentList[i].richContent;
              if (richContent) {
                var parseKey = 'content' + i;
                WxParse.wxParse(parseKey, 'html', richContent, that, 5);
                var pdata = that.data[parseKey].nodes;
                contentList[i].richContentParse = { wxParseData: pdata };
              }

            }
          }
        }


        if (topicDetail && topicDetail.commonKnowledgeContent && topicDetail.commonKnowledgeContent.contentList && topicDetail.commonKnowledgeContent.contentList.length) {
          var contentList = topicDetail.commonKnowledgeContent.contentList;
          for (var i = 0; i < contentList.length; i++) {
            var item = contentList[i];
            if (item.contentType == 2) {
              var thumb = item.soundLength;
              var formatduration = util.formarAudioTime(thumb);
              item.nowduration = '0"';
              item.duration = thumb * 1000;
              item.formatduration = formatduration;
              item.context = item.context;
            }
            if (item.contentType == 3) {
              var videoWidth = item.width;
              var videoHeight = item.height;
              if (videoWidth > videoHeight) {
                videoHeight = videoHeight / 3;
                videoWidth = videoWidth / 3;
              } else {
                videoHeight = videoHeight / 3.5;
                videoWidth = videoWidth / 3.5;
              }
              if (videoWidth > 240) {
                videoHeight = videoHeight / (videoWidth / 240);
                videoWidth = 240;
              }
              item.videoWidth = videoWidth;
              item.videoHeight = videoHeight;


            }
            if (contentList[i].contentType === 4) {
              var richContent = contentList[i].richContent;
              if (richContent) {
                var parseKey = 'commonKnowledgeContent' + i;
                WxParse.wxParse(parseKey, 'html', richContent, that, 5);
                var pdata = that.data[parseKey].nodes;
                contentList[i].richContentParse = { wxParseData: pdata };
              }

            }
          }
        }


        if (topicDetail && topicDetail.practiceContent && topicDetail.practiceContent.contentList && topicDetail.practiceContent.contentList.length) {
          var contentList = topicDetail.practiceContent.contentList;
          for (var i = 0; i < contentList.length; i++) {
            var item = contentList[i];
            if (item.contentType == 2) {
              var thumb = item.soundLength;
              var formatduration = util.formarAudioTime(thumb);
              item.nowduration = '0"';
              item.duration = thumb * 1000;
              item.formatduration = formatduration;
              item.context = item.context;
            }
            if (item.contentType == 3) {
              var videoWidth = item.width;
              var videoHeight = item.height;
              if (videoWidth > videoHeight) {
                videoHeight = videoHeight / 3;
                videoWidth = videoWidth / 3;
              } else {
                videoHeight = videoHeight / 3.5;
                videoWidth = videoWidth / 3.5;
              }
              if (videoWidth > 240) {
                videoHeight = videoHeight / (videoWidth / 240);
                videoWidth = 240;
              }
              item.videoWidth = videoWidth;
              item.videoHeight = videoHeight;


            }
            if (contentList[i].contentType === 4) {
              var richContent = contentList[i].richContent;
              if (richContent) {
                var parseKey = 'practiceContent' + i;
                WxParse.wxParse(parseKey, 'html', richContent, that, 5);
                var pdata = that.data[parseKey].nodes;
                contentList[i].richContentParse = { wxParseData: pdata };
              }

            }
          }
        }


       
        that.setData({
          show:true,
          inChannel: res.inChannel,
          topicDetail: topicDetail,
          onlineChannelInfoDTO: res.onlineChannelInfoDTO,
          pageInfo: pageInfo,
          discoveryDoPlusList: alldiscoveryDoPlusList
        })
      }
    })
  },
  likeDoPlus: function (islikeDoPlus, doPlusId, index) {
    var that = this;
    var discoveryDoPlusList = that.data.discoveryDoPlusList;
    // var islikeDoPlus = !e.currentTarget.dataset.ilike;
    // var doPlusId = e.currentTarget.dataset.doplusid;
    // var index = e.currentTarget.dataset.index;

    var likeDoPlusDTO = {
      doPlusId: doPlusId,
      islikeDoPlus: islikeDoPlus
    };
    util.wxRequest({
      url: 'likeDoPlus',
      data: { likeDoPlusDTO: likeDoPlusDTO },
      success: function (res) {
        discoveryDoPlusList[index].doPlusShowBaseInfo.ilike = islikeDoPlus;
        var likeCount = discoveryDoPlusList[index].doPlusShowBaseInfo.likeCount;
        if (islikeDoPlus) {
          likeCount++;
        } else {
          likeCount--;
        }
        discoveryDoPlusList[index].doPlusShowBaseInfo.likeCount = likeCount;
        that.setData({
          discoveryDoPlusList: discoveryDoPlusList
        });
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    if (that.handleOnShow) {
      that.handleOnShow = false;
      return;
    }
    this.setData({
      pageInfo: {
        pageNum: 1,
        pageSize: 10
      }
    });
    var token = wx.getStorageSync('token');
    if (token) {
      this.init();
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    // 页面退出时，停止播放当前的音频
    var audioPlayer = wx.getBackgroundAudioManager()

    if (audioPlayer.duration && !audioPlayer.paused) {
      audioPlayer.stop()
    }
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    console.log('fuck 111')
    var pageInfo = that.data.pageInfo;
    if (pageInfo && pageInfo.size >= pageInfo.pageNum) {
      that.init();
    }
  },
  onPageScroll: function (e) {
    var that = this;
    var scrollTop = e.scrollTop;

    wx.createSelectorQuery().select('#js_header').boundingClientRect(function (rect) {
      var top = rect.height;
      var tabclass = '';
      if (scrollTop > top) {
        tabclass = 'fixed';
      }
      that.setData({
        tabclass: tabclass
      })
    }).exec()

    wx.createSelectorQuery().select('#js_header').boundingClientRect().select('#js_content').boundingClientRect().select('#js_practice').boundingClientRect().select('#js_doplus').boundingClientRect().exec(function (res) {
      var tab = 0;
      if (scrollTop > res[0].height + res[1].height + res[2].height - 120) {
        tab = 3;
      } else if (scrollTop > res[0].height + res[1].height - 120) {
        tab = 2;
      } else if (scrollTop > res[0].height - 120) {
        tab = 1;
      }
      if (tab) {
        that.setData({
          tab: tab
        })
      }
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    var channelId = this.data.channelId;
    var topicId = this.data.topicId;
    return {
      title: this.data.topicDetail.topicTitle || '玩法详情',
      path: 'pages/discover/pages/channelinfo/playdetail?channelId=' + channelId + '&topicId=' + topicId,
      success: function (res) {
        // 转发成功
        console.log('onShareAppMessage');
        console.log(res.shareTickets);
        if (res.shareTickets) {
          wx.getShareInfo({
            shareTicket: res.shareTickets[0],
            success(res) {
              console.log(res);
              var encryptedData = res.encryptedData;
              var iv = res.iv;
              util.wxRequest({
                url: 'updateGroupInfo',
                data: { encryptedData: encryptedData, iv: iv },
                success: function (res) {
                  console.log(res);
                }
              })
            }
          })
        }
      },
      fail: function (res) {
        // 转发失败
      }
    }
  }
  
})