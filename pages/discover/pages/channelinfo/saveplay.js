import util from '../../../../utils/util';
const Promise = require('../../../common/libs/es6-promise');
const _ = require('../../../common/libs/lodash.core.min.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    isEdit:false,
    saveclass:'unsave',
    topicTitleLen:0,
    topicTitle:'',
    topicDesc:'',
    topicPicUrl:'',
    channelId: '',
    topicType:'',
    topicOrder:'',
    contentList:[],
    isAudio:false,
    audioTime:0,
    delta:2
  },
  chooseImg:function(){
    var that = this;
    wx.chooseImage({
      count: 1,
      success: function (res) {
        var src = res.tempFilePaths[0];
        wx.showLoading();
        util.upload(src, function(res){
          that.setData({
            'topicPicUrl': res.imageURL,
          });
          that.checkSaveClass();
          wx.hideLoading();
        }, function(){
          wx.hideLoading();
        });

      }

    })
  },
  bindInputTopicTitle:function(e){
    var value = e.detail.value.trim();
    this.setData({
      topicTitle: value,
      topicTitleLen: value.length
    })
    this.checkSaveClass();
  },
  checkSaveClass:function(){
    var topicTitle = this.data.topicTitle;
    var topicOrder = this.data.topicOrder;
    var topicPicUrl = this.data.topicPicUrl;
    var topicType = this.data.topicType;
    var saveclass = 'unsave';
    if (topicType == 0){
      if (topicTitle && topicOrder && topicPicUrl) {
        saveclass = 'save';
      }
    }else{
      if (topicTitle && topicPicUrl) {
        saveclass = 'save';
      }
    }
    
    this.setData({
      saveclass: saveclass
    })
  },
  addTextList:function(){
    var contentList = this.data.contentList;
    contentList.push({ topicContentType: 0, context: '', thumb:''});
    this.setData({
      contentList: contentList
    })
  },
  upload:function(type,src,res){
    var that = this;
    wx.showLoading();
    util.upload(src, function(resp){
      if(type == 1){
        var contentList = that.data.contentList;
        contentList.push({ topicContentType: type, context: resp.imageURL, thumb: '' });
        that.setData({
          contentList: contentList
        })
         wx.hideLoading();
      }else if(type == 3){
        var contentList = that.data.contentList;
        var src = resp.imageURL;
        wx.request({
          url: src+'?avinfo',
          success:function(res){
            console.log(res);
            var vwidth = 320;
            var vheight = 182;
            if(res && res.data && res.data.streams && res.data.streams.length){
              var streams = res.data.streams;
              vwidth= streams[0].width/3 || streams[1].width/1.5;
              vheight = streams[0].height/3 || streams[1].height/1.5;
            }
            contentList.push({ topicContentType: type, context: resp.imageURL, thumb: '',vwidth:vwidth,vheight:vheight });
            that.setData({
              contentList: contentList
            })

            wx.hideLoading();
          },complete:function(){
            wx.hideLoading();
          }
        });
      }else if(type == 2){
        var duration = res.duration;
        var thumb = Math.ceil(duration);
        var formatduration = util.formarAudioTime(thumb / 1000);
        var contentList = that.data.contentList;
        contentList.push({ topicContentType: 2, context: resp.imageURL, thumb: thumb, isPlay: false, duration: duration, nowduration: '0“', formatduration: formatduration });
        that.setData({
          isAudio: false,
          audioTime: 0,
          contentList: contentList
        })
         wx.hideLoading();
      }
     
    }, function(){
      wx.hideLoading();
    });

  },
  addImgList: function () {
    var that = this;
    // 选择图片
    wx.chooseImage({
      count: 1,
      success: function (res) {
        var src = res.tempFilePaths[0];
        that.upload(1,src);   
      }
    })

    
  },
  playAudio:function(e){
    var that = this;
    var index = e.currentTarget.dataset.index;
    var contentList = this.data.contentList;
    var isPlay = contentList[index].isPlay;
    var duration = contentList[index].duration;
    
    var src = contentList[index].context;
    that.innerAudioContext.src = src;
    that.innerAudioContext.onPlay(() => {
      console.log('开始播放');
      contentList[index].isPlay = true;
      that.setData({
        contentList: contentList
      })
    })
    that.innerAudioContext.onPause(() => {
      console.log('暂停');
      contentList[index].isPlay = false;
      that.setData({
        contentList: contentList
      })
    })
    that.innerAudioContext.onStop(() => {
      console.log('停止')
      contentList[index].isPlay = false;
      that.setData({
        contentList: contentList
      })
    })
    that.innerAudioContext.onEnded(() => {
      console.log('结束')
      contentList[index].isPlay = false;
      contentList[index].progress = 100;
      that.setData({
        contentList: contentList
      })
    })
    


    if (isPlay) {
      contentList[index].isPlay = false;
      that.setData({
        contentList: contentList
      })
      that.innerAudioContext.pause();
    } else {
      that.innerAudioContext.play();
      that.innerAudioContext.onTimeUpdate(() => {
        var currentTime = that.innerAudioContext.currentTime;
        contentList[index].progress = currentTime * 1000 / duration * 100;
        contentList[index].nowduration = util.formarAudioTime(currentTime);
        that.setData({
          contentList: contentList
        })
      })
    }
  },
  addAudioList: function () {
    var timer;
    var that = this;
    var isAudio = that.data.isAudio;
    
    const options = {
      duration: 600000,
      sampleRate: 44100,
      numberOfChannels: 1,
      encodeBitRate: 192000,
      format: 'aac',
      frameSize: 50
    }
    if (isAudio) {
      this.recorderManager.stop();
    } else {
      this.recorderManager.start(options)
    }
  },
  stopAudio:function(){

  },
  addVedioList: function () {
    var that = this;
    var contentList = that.data.contentList;
    wx.chooseVideo({
      sourceType: ['album', 'camera'],
      maxDuration: 60,
      camera: 'back',
      success: function (res) {
        if(res && res.size && res.size > 10*1024*1024){
          util.showToast('文件过大，请重新上传');
          return;
        }
        var src = res.tempFilePath;
        that.upload(3,src);
      }
    })
    
  },
  upHandle:function(e){
    var disabled = e.currentTarget.dataset.disabled;
    if (disabled){
      return;
    }
    var index = e.currentTarget.dataset.index;
    var contentList = this.data.contentList;
    contentList[index] = contentList.splice(index - 1, 1, contentList[index])[0];
    this.setData({
      contentList: contentList
    })
  },
  downHandle:function(e){
    var disabled = e.currentTarget.dataset.disabled;
    if (disabled) {
      return;
    }
    var index = e.currentTarget.dataset.index;
    var contentList = this.data.contentList;
    contentList[index] = contentList.splice(index + 1, 1, contentList[index])[0];
    this.setData({
      contentList: contentList
    })
  },
  delHandle:function(e){
    var index = e.currentTarget.dataset.index;
    var contentList = this.data.contentList;
    contentList.splice(index,1);
    this.setData({
      contentList: contentList
    })
  },
  bindDateChange:function(e){
    this.setData({
      topicOrder:e.detail.value
    })
    this.checkSaveClass()
  },
  save:function(){
    var that = this;
    var saveclasss = this.data.saveclasss;
    if (saveclasss == 'unsave'){
      return;
    }
    var data = that.data;
    var contentList = data.contentList;
    var param = {
      topic:{
        topicType: data.topicType,
        topicTitle: data.topicTitle,
        topicOrder: data.topicOrder,
        channelId: data.channelId,
        topicPicUrl: data.topicPicUrl,
        contentList: contentList
      }
    };
    var url = 'addChannelTopic';
    if(this.data.isEdit){
      url = 'modifyChannelTopic';
      var topicId = that.data.topicId;
      param.topic.topicId = topicId;
    }
    util.wxRequest({
      url:url,
      data: param,
      success:function(res){
        that.refreshPrevPage();
        var delta = that.data.delta;
        wx.navigateBack({
          delta: delta
        })
      },
      onfail:function(res){
        console.log(res);
        if (res && res.code == 'topic_date_00002'){
          util.showToast('当前日期玩法已存在')
        }
      }
    })
  },
  refreshPrevPage: function () {
    var pages = getCurrentPages();
    if (pages.length > 1) {
      var prePage = pages[pages.length - 3];
      prePage.init();
      var prePage2 = pages[pages.length - 2];
      prePage2.init();
    }
  },
  bindInputContext:function(e){
    var vlaue = e.detail.value;
    var index = e.currentTarget.dataset.index;
    var contentList = this.data.contentList;
    contentList[index] = { topicContentType: 0, context:vlaue, thumb: '' };
    this.setData({
      contentList: contentList
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    this.setData({
      topicType: options.topicType,
      channelId: options.channelId,
      topicId: options.topicId,
      isEdit: options.isEdit || false,
      delta: options.delta || 2
    })
    if (options.isEdit){
      this.initTopic(options.topicId);
    }


    that.recorderManager = wx.getRecorderManager();
    that.recorderManager.onStart(() => {
      console.log('recorder start')
      that.setData({
        isAudio: true
      })
      that.timer = setInterval(function () {
        var audioTime = that.data.audioTime;
        that.setData({
          audioTime: audioTime + 1
        })
      }, 1000);
    })
    that.recorderManager.onResume(() => {
      console.log('recorder resume')
    })
    that.recorderManager.onPause(() => {
      console.log('recorder pause')
    })
    that.recorderManager.onStop((res) => {
      console.log('recorder stop', res)
      clearInterval(that.timer);
      that.upload(2, res.tempFilePath, res);
    })
    that.recorderManager.onFrameRecorded((res) => {
      const { frameBuffer } = res
      console.log('frameBuffer.byteLength', frameBuffer.byteLength)
    })

    this.innerAudioContext = wx.createInnerAudioContext();
    this.innerAudioContext.onError((res) => {
      that.tip("播放录音失败！")
    })
  },
  tip: function (msg) {
    wx.showModal({
      title: '提示',
      content: msg,
      showCancel: false
    })
  },
  initTopic: function (topicId){
    var that = this;
    util.wxRequest({
      url: 'getChannelTopicDetail',
      data: { topicId: topicId },
      success: function (res) {
        var topic = res.topic;
        var contentList = topic.contentList;
        for (var i = 0; i < contentList.length; i++) {
          var item = contentList[i];
          if (item.topicContentType == 2) {
            var thumb = item.thumb;
            var formatduration = util.formarAudioTime(thumb/1000);
            item.nowduration = '0"';
            item.duration = thumb;
            item.formatduration = formatduration;
          }
        }
        that.setData({
          topicOrder: topic.topicOrder,
          topicPicUrl: topic.topicPicUrl,
          topicTitle: topic.topicTitle,
          topicTitleLen: topic.topicTitle.length,
          contentList: topic.contentList
        })
        that.checkSaveClass();
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  }
})