import util from '../../../../utils/util';
var WxParse = require('../../../../wxParse/wxParse.js');
var app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    audioPlayer: null,
    show:false,
    tab:1,
    tabclass:'',
    topicStatus:1,
    userRole: '',
    topicId:'',
    topicList: [],
    picBookInfo:{},
    topicSum:{},
    discoveryDoPlusList:[],
    channelId:'',
    inChannel:false,
    channelMemberCount:'',
    channelDoPlusCount:'',
    accumulateDoPlus:'',
    pageInfo: {
      pageNum: 1,
      pageSize: 10
    }
  },
  scrollToDesc:function(){
    var that = this;
    var tab = that.data.tab;
    that.setData({
      tab: 1
    })
    wx.createSelectorQuery().select('#js_desc').boundingClientRect().selectViewport().scrollOffset().exec(function (res) {
      wx.pageScrollTo({
        scrollTop: res[0].top + res[1].scrollTop-50,
        duration: 300
      })
    })
  },
  scrollToTopic: function () {
    var that = this;
    var tab = that.data.tab;
    that.setData({
      tab: 2
    })
    wx.createSelectorQuery().select('#js_topic').boundingClientRect().selectViewport().scrollOffset().exec(function (res) {
      wx.pageScrollTo({
        scrollTop: res[0].top + res[1].scrollTop-50,
        duration: 300
      })
    })
  },
  scrollToDoplus: function () {
    var that = this;
    var tab = that.data.tab;
    that.setData({
      tab: 3
    })
    wx.createSelectorQuery().select('#js_doplus').boundingClientRect().selectViewport().scrollOffset().exec(function (res) {
      wx.pageScrollTo({
        scrollTop: res[0].top + res[1].scrollTop-50,
        duration: 300
      })
    })
  },
  changeTopicType:function(){
    var topicStatus = this.data.topicStatus;
    var topicStatus = topicStatus === 1 ? 0 : 1;
    this.setData({
      topicStatus: topicStatus,
      pageInfo: {
        pageNum: 1,
        pageSize: 10
      }
    })
    this.init();
  },
  getUserInfoHandler: function (e) {
    var that = this;
    var userInfo = e.detail.userInfo;
    util.setUserInfoHandler(userInfo, function () {
      that.joinChannel(e);
    });
  },
  onPageScroll:function(e){
    var that = this;
    var scrollTop = e.scrollTop;
    var realWindowHeight = that.data.realWindowHeight;
    if (scrollTop > 300){
      this.setData({
        showscrollview:true
      })
    }else{
      this.setData({
        showscrollview: false
      })
    }

  
    
    wx.createSelectorQuery().select('#js_header').boundingClientRect(function (rect) {
      var top = rect.height;
      var tabclass = '';
      if (scrollTop > top){
        tabclass = 'fixed';
      }
      that.setData({
        tabclass: tabclass
      })
    }).exec()
    

    wx.createSelectorQuery().select('#js_header').boundingClientRect().select('#js_desc').boundingClientRect().select('#js_topic').boundingClientRect().select('#js_doplus').boundingClientRect().exec(function (res) {
      var tab = 0;
      if (scrollTop > res[0].height + res[1].height + res[2].height - 60) {
        tab = 3;
      } else if (scrollTop > res[0].height + res[1].height - 60){
        tab = 2;
      } else if (scrollTop > res[0].height - 60) {
        tab = 1;
      }
      if (tab) {
        that.setData({
          tab: tab
        })
      }
    })

  

   

  },
  getUserInfoByDolikeHandler: function (e) {
    var that = this;
    var islikeDoPlus = !e.currentTarget.dataset.ilike;
    var doPlusId = e.currentTarget.dataset.doplusid;
    var index = e.currentTarget.dataset.index;

    var userInfo = e.detail.userInfo;
    util.setUserInfoHandler(userInfo, function () {
      that.likeDoPlus(islikeDoPlus, doPlusId, index);
    });
  },
  getUserInfoByCommentHandler: function (e) {
    var that = this;
    var userInfo = e.detail.userInfo;
    var doPlusId = e.currentTarget.dataset.doplusid;
    util.setUserInfoHandler(userInfo, function () {
      that.gododetail(doPlusId);
    });
  },
  likeDoPlus: function (islikeDoPlus, doPlusId, index) {
    var that = this;
    var discoveryDoPlusList = that.data.discoveryDoPlusList;
    // var islikeDoPlus = !e.currentTarget.dataset.ilike;
    // var doPlusId = e.currentTarget.dataset.doplusid;
    // var index = e.currentTarget.dataset.index;

    var likeDoPlusDTO = {
      doPlusId: doPlusId,
      islikeDoPlus: islikeDoPlus
    };
    util.wxRequest({
      url: 'likeDoPlus',
      data: { likeDoPlusDTO: likeDoPlusDTO },
      success: function (res) {
        discoveryDoPlusList[index].doPlusShowBaseInfo.ilike = islikeDoPlus;
        var likeCount = discoveryDoPlusList[index].doPlusShowBaseInfo.likeCount;
        if (islikeDoPlus){
          likeCount++; 
        }else{
          likeCount--;
        }
        discoveryDoPlusList[index].doPlusShowBaseInfo.likeCount = likeCount;
        that.setData({
          discoveryDoPlusList: discoveryDoPlusList
        });
      }
    })
  },
  preImage: function (e) {
    this.handleOnShow = true;
    var current = e.currentTarget.dataset.src;
    var index = e.currentTarget.dataset.index;
    var urls = this.data.discoveryDoPlusList[index].doPlusShowBaseInfo.doPlusContent.imageContent;
    wx.previewImage({
      current: current, // 当前显示图片的http链接
      urls: urls // 需要预览的图片http链接列表
    })
  },
  gotop:function(){
    wx.pageScrollTo({
      scrollTop: 0
    })
  },
  gohome:function(){
    wx.switchTab({
      url: '/pages/tabBar/discover/index'
    })
  },
  gododetail: function (doPlusId){
    // var doPlusId = e.currentTarget.dataset.doplusid;
    var url = '../../../home/pages/detail/detail?doPlusId=' + doPlusId+'&from=channel';
    wx.navigateTo({
      url: url
    })
  },
  gododetailInfo: function (e) {
    var doPlusId = e.currentTarget.dataset.doplusid;
    var url = '../../../home/pages/detail/detail?doPlusId=' + doPlusId + '&from=channel';
    wx.navigateTo({
      url: url
    })
  },
  playTopicVideo: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var topic = this.data.topic;
    topic.contentList[index].showVedio = true;
    that.setData({
      topic: topic
    })

  },
  playTopicAudio: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var topic = this.data.topic;
    var isPlay = topic.contentList[index].isPlay;
    var duration = topic.contentList[index].thumb;

    var src = topic.contentList[index].context;
    that.innerAudioContext.src = src;
    that.innerAudioContext.onPlay(() => {
      console.log('开始播放');
      topic.contentList[index].isPlay = true;
      that.setData({
        topic: topic
      })
    })
    that.innerAudioContext.onPause(() => {
      console.log('暂停');
      topic.contentList[index].isPlay = false;
      that.setData({
        topic: topic
      })
    })
    that.innerAudioContext.onStop(() => {
      console.log('停止')
      topic.contentList[index].isPlay = false;
      that.setData({
        topic: topic
      })
    })
    that.innerAudioContext.onEnded(() => {
      console.log('结束')
      topic.contentList[index].isPlay = false;
      topic.contentList[index].progress = 100;
      that.setData({
        topic: topic
      })
    })


    if (isPlay) {
      topic.contentList[index].isPlay = false;
      that.setData({
        topic: topic
      })
      that.innerAudioContext.pause();
    } else {
      that.innerAudioContext.play();
      that.innerAudioContext.onTimeUpdate(() => {
        var currentTime = that.innerAudioContext.currentTime;
        topic.contentList[index].progress = currentTime * 1000 / duration * 100;
        topic.contentList[index].nowduration = util.formarAudioTime(currentTime);
        that.setData({
          topic: topic
        })
      })
    }
  },
  playVedioBookInfo: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var picBookInfo = this.data.picBookInfo;
    for (var i = 0; i < picBookInfo.channelDescription.contentList.length;i++){
      picBookInfo.channelDescription.contentList[i].showVedio = false;
    }
    picBookInfo.channelDescription.contentList[index].showVedio = true;
    that.setData({
      picBookInfo: picBookInfo
    })
    that.stopAudio();
  },
  playVedio: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var discoveryDoPlusList = this.data.discoveryDoPlusList;
    for (var i = 0; i < discoveryDoPlusList.length;i++){
      discoveryDoPlusList[i].showVedio = false;
    }
    discoveryDoPlusList[index].showVedio = true;
    that.setData({
      discoveryDoPlusList: discoveryDoPlusList
    })
    that.stopAudio();
  },
  startPlayAudio: function (src, index) {
    var that = this;
    var audioPlayer = wx.getBackgroundAudioManager();
    var contentList = this.data.discoveryDoPlusList;

    if (audioPlayer.duration && !audioPlayer.paused) {
      audioPlayer.pause();

      if (audioPlayer.src === src && audioPlayer.currentTime >= audioPlayer.duration) {
        audioPlayer.src = src + `?_t=${ +new Date() }`
        return;
      } else if (audioPlayer.src === src) {
        audioPlayer.play();
        return;
      } else {
        contentList.forEach(list => {
          if (list.isPlay) {
            list.isPlay = false;
            list.progress = 0;
            list.doPlusShowBaseInfo.nowduration = 0;
          }
        })
        this.setData({
          discoveryDoPlusList: contentList
        })
      }
    } else if (audioPlayer.duration && audioPlayer.paused) {
      if (audioPlayer.src === src) {
        audioPlayer.play();
        return;
      } else {
        contentList.forEach(list => {
          if (Object.keys(list).indexOf('isPlay') !== -1) {
            list.isPlay = false;
            list.progress = 0;
            list.doPlusShowBaseInfo.nowduration = 0;
          }
        })
        this.setData({
          discoveryDoPlusList: contentList
        })
      }
    }
    audioPlayer.src = src;
    audioPlayer.title = '卡乐猫喵喵乐园';
    audioPlayer.play()

    audioPlayer.onWaiting(() => {
      console.log('waiting')
    })
    audioPlayer.onTimeUpdate(() => {
      console.log('timeUpdate')
      var currentTime = audioPlayer.currentTime;
      var duration = audioPlayer.duration;
      contentList[index].progress = currentTime / duration * 100;
      contentList[index].doPlusShowBaseInfo.nowduration = util.formarAudioTime(currentTime);
      that.setData({
        discoveryDoPlusList: contentList
      })
    })
    audioPlayer.onPlay(() => {
      console.log('开始播放');
      contentList[index].isPlay = true;
      that.setData({
        discoveryDoPlusList: contentList
      })
    })
    audioPlayer.onPause(() => {
      console.log('暂停');
      contentList[index].isPlay = false;
      that.setData({
        discoveryDoPlusList: contentList
      })
    })
    audioPlayer.onStop(() => {
      console.log('停止')
      contentList[index].isPlay = false;
      that.setData({
        discoveryDoPlusList: contentList
      })
    })
    audioPlayer.onEnded(() => {
      console.log('结束')
      contentList[index].isPlay = false;
      contentList[index].progress = 0;
      that.setData({
        discoveryDoPlusList: contentList
      })
    })
  },
  pauseAudio: function () {
    var audioPlayer = wx.getBackgroundAudioManager()
    audioPlayer.pause()
  },
  playAudio: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var contentList = this.data.discoveryDoPlusList;

    var src = contentList[index].doPlusShowBaseInfo.doPlusContent.soundContent.soundUrl;
    that.startPlayAudio(src, index)
  },
  playAudioBookInfo: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var contentList = this.data.picBookInfo.channelDescription.contentList;
    var isPlay = contentList[index].isPlay;
    var duration = contentList[index].duration;
    var audioPlayer = wx.getBackgroundAudioManager();

    var src = contentList[index].url;
    var picBookInfo = this.data.picBookInfo;
    audioPlayer.src = src;
    audioPlayer.title = '卡乐猫喵喵乐园';
    audioPlayer.onPlay(() => {
      console.log('开始播放');
      contentList[index].isPlay = true;
      picBookInfo.channelDescription.contentList = contentList;
      that.setData({
        picBookInfo: picBookInfo
      })
    })
    audioPlayer.onPause(() => {
      console.log('暂停');
      contentList[index].isPlay = false;
      picBookInfo.channelDescription.contentList = contentList;
      that.setData({
        picBookInfo: picBookInfo
      })
    })
    audioPlayer.onStop(() => {
      console.log('停止')
      contentList[index].isPlay = false;
      picBookInfo.channelDescription.contentList = contentList;
      that.setData({
        picBookInfo: picBookInfo
      })
    })
    audioPlayer.onEnded(() => {
      console.log('结束')
      contentList[index].isPlay = false;
      contentList[index].progress = 0;
      picBookInfo.channelDescription.contentList = contentList;
      that.setData({
        picBookInfo: picBookInfo
      })
    })


    if (isPlay) {
      contentList[index].isPlay = false;
      picBookInfo.channelDescription.contentList = contentList;
      that.setData({
        picBookInfo: picBookInfo
      })
      audioPlayer.pause();
    } else {
      audioPlayer.play();
      audioPlayer.onTimeUpdate(() => {
        var currentTime = audioPlayer.currentTime;
        contentList[index].progress = currentTime * 1000 / duration * 100;
        contentList[index].nowduration = util.formarAudioTime(currentTime);
        picBookInfo.channelDescription.contentList = contentList;
        that.setData({
          picBookInfo: picBookInfo
        })
      })
    }
  },
  changeAudioProgress: function (e) {
    var that = this;
    var value = e.detail.value;
    var index = e.currentTarget.dataset.index;
    var contentList = this.data.discoveryDoPlusList;

    var audioPlayer = wx.getBackgroundAudioManager();
    var duration = audioPlayer.duration;

    if (!duration) {
      var src = contentList[index].doPlusShowBaseInfo.doPlusContent.soundContent.soundUrl;
      that.startPlayAudio(src, index)
      duration = audioPlayer.duration;
    }

    var targetTime = duration * (value / 100);

    if (audioPlayer.paused) {
      audioPlayer.play()
    }
    wx.seekBackgroundAudio({
      position: targetTime,
      success: function() {
        var currentTime = audioPlayer.currentTime;
        contentList[index].progress = currentTime * 1000 / duration * 100;
        that.setData({
          discoveryDoPlusList: contentList
        })
      }
    })
  },
  showall:function(e){
    var index = e.currentTarget.dataset.index;
    var discoveryDoPlusList = this.data.discoveryDoPlusList;
    discoveryDoPlusList[index].doPlusShowBaseInfo.showall = false;
    discoveryDoPlusList[index].doPlusShowBaseInfo.textContentPreview = discoveryDoPlusList[index].doPlusShowBaseInfo.doPlusContent.textContent;
    this.setData({
      discoveryDoPlusList: discoveryDoPlusList
    })
  },
  init: function (){
    var that = this;
    var pageInfo = that.data.pageInfo;
    var channelId = that.data.channelId;
    var topicStatus = that.data.topicStatus;
    var isHideLoading = false;
    if(that.data.show){
      isHideLoading = false;
    }
    util.wxRequest({
      url:'getChannelDetailPageInfoV2',
      data:{
        channelId: channelId,
        topicStatus: topicStatus,
        pageInfo: pageInfo
      },
      success:function(res){
        var pageInfo = res.pageInfo || {};
        pageInfo.pageNum++;
        var picBookInfo = res.picBookInfo;
        var topicList = res.topicList;
        var discoveryDoPlusList = res.discoveryDoPlusList || [];
        if (discoveryDoPlusList && discoveryDoPlusList.length){
          for (var i = 0; i < discoveryDoPlusList.length;i++){
            var item = discoveryDoPlusList[i].doPlusShowBaseInfo;
            var textContentPreview = item.textContentPreview;
            var textContent = item.doPlusContent.textContent;
            if (textContentPreview && textContent && textContentPreview != textContent){
              item.showall = true;
            }
            if (item.doPlusContent.soundContent){
              var thumb = item.doPlusContent.soundContent.soundSeconds/1000;
              var formatduration = util.formarAudioTime(thumb);
              item.nowduration = '0"';
              item.duration = thumb*1000;
              item.formatduration = formatduration;
              item.context = item.doPlusContent.soundContent.soundUrl;
            }

            if (item.doPlusContent.videoContent) {
              var videoWidth = item.doPlusContent.videoContent.videoWidth; 
              var videoHeight = item.doPlusContent.videoContent.videoHeight;
              var videoSize = util.getVideoSize(videoWidth, videoHeight);
              videoWidth = videoSize.videoWidth;
              videoHeight = videoSize.videoHeight;
              item.doPlusContent.videoContent.videoWidth = videoWidth;
              item.doPlusContent.videoContent.videoHeight = videoHeight;

            }

          }
        }

        if (picBookInfo && picBookInfo.channelDescription && picBookInfo.channelDescription.contentList && picBookInfo.channelDescription.contentList.length){
          var contentList = picBookInfo.channelDescription.contentList;
          for (var i = 0; i < contentList.length; i++) {
            var item = contentList[i];
            if (item.contentType  == 2) {
              var thumb = item.soundLength;
              var formatduration = util.formarAudioTime(thumb);
              item.nowduration = '0"';
              item.duration = thumb * 1000;
              item.formatduration = formatduration;
              item.context = item.context;
            }
            if (item.contentType == 3) {
              var videoWidth = item.width;
              var videoHeight = item.height;
              if (videoWidth > videoHeight) {
                videoHeight = videoHeight / 3;
                videoWidth = videoWidth / 3;
              } else {
                videoHeight = videoHeight / 3.5;
                videoWidth = videoWidth / 3.5;
              }
              if (videoWidth > 240) {
                videoHeight = videoHeight / (videoWidth / 240);
                videoWidth = 240;
              }
              item.videoWidth = videoWidth;
              item.videoHeight = videoHeight;

            
            }

          }
          
        }
        var alldiscoveryDoPlusList = discoveryDoPlusList;
        if (pageInfo.pageNum > 2){
          alldiscoveryDoPlusList = that.data.discoveryDoPlusList.concat(discoveryDoPlusList);
        }


        if (picBookInfo && picBookInfo.channelDescription && picBookInfo.channelDescription.contentList && picBookInfo.channelDescription.contentList.length){
          var contentList = picBookInfo.channelDescription.contentList;
          for (var i = 0; i < contentList.length; i++) {
            if (contentList[i].contentType===4){
              var richContent = contentList[i].richContent;
              if (richContent){
                var parseKey = 'richContent'+i;
                WxParse.wxParse(parseKey, 'html', richContent, that, 5);
                var pdata = that.data[parseKey].nodes;
                contentList[i].richContentParse = {wxParseData:pdata};
              }

            }
          }

        }
        
        var topicId = '';
        if (topicList && topicList.length){
          topicId = topicList[topicList.length - 1].topicId;
        }
        that.setData({
          picBookInfo: picBookInfo,
          topicList: topicList,
          topicSum: res.topicSum,
          discoveryDoPlusList: alldiscoveryDoPlusList,
          pageInfo: pageInfo,
          inChannel: res.inChannel,
          channelMemberCount: res.channelMemberCount,
          channelDoPlusCount: res.channelDoPlusCount,
          accumulateDoPlus: res.accumulateDoPlus,
          topicId:topicId,
          show:true
        })
        



      },
      onfail:function(){

      },
      isHideLoading:isHideLoading
    })
  },
  closeMask:function(){
    this.setData({
      showParticipantList: false
    })
  },
  selpartner:function(e){
    var that = this;
    var index = e.currentTarget.dataset.index;
    var pid = e.currentTarget.dataset.pid;
    var participantList = this.data.participantList;
    participantList[index].statusimg = '../../../../images/selpartner.png';
    this.setData({
      participantList:participantList
    })
    var channelId = this.data.channelId;
    util.wxRequest({
      url: 'joinChannel',
      data: { participantId:pid,channelId,channelId},
      success: function (res) {
        that.setData({
          inChannel:true,
          showParticipantList: false,
        })

      }
    })

  },
  updateChannel: function () {
    var that = this;
    var data = that.data;
    var participantList = data.participantList;
    var participantId;
    for (var i = 0;i<participantList.length;i++){
      if (participantList[i].statusimg){
        participantId = participantList[i].pid;
      }
    }
    if (!participantId){
      that.closesettingbox();
      return;
    }
    var channelInfo = {
      channelId: data.channelId,
      participantId: participantId
    };
    util.wxRequest({
      url: 'modifyChannelInfo',
      data: { channelInfo: channelInfo },
      success: function (res) {
        that.closesettingbox();
      }
    })
  },
  updatepartner: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var pid = e.currentTarget.dataset.pid;
    var participantList = this.data.participantList;
    participantList[index].statusimg = '../../../../images/selpartner.png';
    this.setData({
      participantList: participantList
    })
  },
  doplus:function(){
    var topic = this.data.topic;
    var channelId = this.data.channelId;
    var url = '../../../home/pages/do/do?channelId=' + channelId;
    wx.navigateTo({
      url: url
    })
    
  },
  joinChannel:function(){
    var that = this;
    // util.wxRequest({
    //   url: 'getMinePageInfo',
    //   success: function (res) {
    //     that.setData({
    //       showParticipantList:true,
    //       participantList: res.participantList
    //     })
    //   }
    // })
    var channelId = this.data.channelId;
    util.wxRequest({
      url: 'joinChannel',
      data: { channelId, channelId },
      success: function (res) {
        that.setData({
          inChannel: true,
        })

      }
    })
  },
  goaddplay:function(){
    var topic = this.data.topic;
    var channelId = this.data.channelId;
    if (topic && (topic.topicType == 0 || topic.topicType ==1) )  {
      var topicType = topic.topicType;
      wx.navigateTo({
        url: 'saveplay?delta=1&topicType=' + topicType + '&channelId=' + channelId,
      })
    } else {
      wx.navigateTo({
        url: 'addplay?channelId=' + channelId
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showShareMenu({
      withShareTicket: true
    });
    var that = this;
    var scene = decodeURIComponent(options.scene);

    var channelId = options.channelId || scene;
    this.setData({
      channelId: channelId,
      audioPlayer: app.globalData.audioPlayer
    })

    this.innerAudioContext = wx.createInnerAudioContext();
    this.innerAudioContext.onError((res) => {
      that.tip("播放录音失败！")
    })

  },
  tip: function (msg) {
    wx.showModal({
      title: '提示',
      content: msg,
      showCancel: false
    })
  },
  stopAudio:function(){
    var that = this;
    that.innerAudioContext.pause();
    var discoveryDoPlusList = that.data.discoveryDoPlusList;
    for (var i = 0; i < discoveryDoPlusList.length;i++){
      discoveryDoPlusList[i].isPlay = false;
    }
    that.setData({
      discoveryDoPlusList: discoveryDoPlusList
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    if (that.handleOnShow){
      that.handleOnShow = false;
      return;
    }
    this.setData({
      userRole: wx.getStorageSync('userRole'),
      pageInfo: {
        pageNum: 1,
        pageSize: 10
      }
    });
    var token = wx.getStorageSync('token');
    if (token) {
      that.init();
    }

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    // 页面退出时，停止播放当前的音频
    var audioPlayer = wx.getBackgroundAudioManager()

    if (audioPlayer.duration && !audioPlayer.paused) {
      audioPlayer.stop()
    }
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var pageInfo = that.data.pageInfo;
    if (pageInfo && pageInfo.size >= pageInfo.pageNum) {
      that.init();
    }
  },
  goplaylist:function(){
    var channelId = this.data.channelId;
    wx.navigateTo({
      url: 'playlist?channelId=' + channelId
    })
  },
  gochannelsetting:function(){
    var that = this;
    var channelId = that.data.channelId;
    var isChannelOwner = that.data.isChannelOwner;    
    var inChannel = that.data.inChannel;    
    if (isChannelOwner && inChannel){
      wx.navigateTo({
        url: '../channelsetting/channelsetting?channelId=' + channelId + '&isChannelOwner='+isChannelOwner
      })
    } else if (inChannel){
      var currentPid = that.data.channelBaseInfo.participantId;
      util.wxRequest({
        url: 'getMinePageInfo',
        success: function (res) {
          var participantList = res.participantList;
          for (var i = 0; i < participantList.length;i++){
            var pid = participantList[i].pid;
            console.log(pid, currentPid)
            if (pid == currentPid){
              participantList[i].statusimg = '../../../../images/selpartner.png';
            }
          }
          that.setData({
            showChannelSetting: true,
            participantList:participantList
          })
        }
      })
    }
  },
  closesettingbox:function(){
    this.setData({
      showChannelSetting:false
    })
  },
  quitChannel: function () {
    var that = this;
    var channelId = this.data.channelId;
    wx.showModal({
      title: '',
      content: '确定要退出频道吗',
      success: function (res) {
        if (res.confirm) {

          util.wxRequest({
            url: 'quitChannel',
            data: { channelId: channelId },
            success: function (res) {
              that.closesettingbox();
              that.init();
            }
          });
        } else if (res.cancel) {

        }
      }
    })


  },
  goplaydetail:function(e){
    // var inChannel = this.data.inChannel;
    // if (!inChannel){
    //   util.showToast('请先加入频道')
    //   return;
    // }
    var channelId = this.data.channelId;
    var topicId = e.currentTarget.dataset.topicid;
    if (!topicId){
      return;
    }
    wx.navigateTo({
      url: 'playdetail?topicId=' + topicId + '&channelId=' + channelId,
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: this.data.picBookInfo.channelName || '探索详情',
      path: 'pages/discover/pages/channelinfo/channelinfo?channelId='+this.data.channelId,
      success: function (res) {
        // 转发成功
        console.log('onShareAppMessage');
        console.log(res.shareTickets);
        if (res.shareTickets) {
          wx.getShareInfo({
            shareTicket: res.shareTickets[0],
            success(res) {
              console.log(res);
              var encryptedData = res.encryptedData;
              var iv = res.iv;
              util.wxRequest({
                url:'updateGroupInfo',
                data: { encryptedData: encryptedData,iv:iv},
                success:function(res){
                  console.log(res);
                }
              })
            }
          })
        }
      },
      fail: function (res) {
        // 转发失败
      }
    }
  }
})