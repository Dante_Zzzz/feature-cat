import util from '../../../../utils/util';
const _ = require('../../../common/libs/lodash.core.min.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    show:false,
    channelId:'',
    topicType:'',
    userStatus:'',
    topicList:[]
  },
 exist:function(obj,year) {
    var flag = false;
    _.each(obj, function (item, i) {
      if (year == i) {
        flag = true;
      }
    })
    return flag;
  },
  groupBy:function(data){
    var that = this;
    var obj = {};
    _.each(data, function (item, i) {
      if (!that.exist(obj,item.year)) {
        obj[item.year] = [item];
      } else {
        var tmp = obj[item.year];
        tmp.push(item)
        obj[item.year] = tmp;
      }
    })
    return obj;
  },
  gosaveplay:function(e){
    var topicId = e.currentTarget.dataset.topicid;
    var topicType = this.data.topicType;
    wx.navigateTo({
      url: 'saveplay?topicId=' + topicId + '&topicType=' + topicType+ '&isEdit='+true
    })
  },
  goaddplay:function(){
    var channelId = this.data.channelId;
    var topicList = this.data.topicList;
    var topicType = this.data.topicType;
    if (topicList && topicList.length>0){
      wx.navigateTo({
        url: 'saveplay?delta=1&topicType=' + topicType + '&channelId=' + channelId, 
      })
    }else{
      wx.navigateTo({
        url: 'addplay?channelId=' + channelId
      })
    }
  },
  goplaydetail:function(e){
    var isFinished = e.currentTarget.dataset.isfinished;
    var topicType = this.data.topicType;
    if (!isFinished && topicType === 1){
      util.showToast('请先完成之前关卡');
      return;
    }
    var topicId = e.currentTarget.dataset.topicid;
    var channelId = this.data.channelId;
    wx.navigateTo({
      url: 'playdetail?topicId=' + topicId + '&channelId=' + channelId,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // var topicType = options.topicType;
    //topicType 0 日期 1 关卡
    var channelId = options.channelId;
    this.setData({
      channelId: channelId
    })
    this.init();
  },
  init:function(){
    var that = this;
    var channelId = that.data.channelId;
    util.wxRequest({
      url:'getChannelTopicList',
      data: { channelId: channelId},
      success:function(res){
        var topicList = res.topicList;
        var userStatus = res.userStatus;
        if (topicList && topicList.length > 0){
          var topicType;
          _.each(topicList,function(item,i){
            var topicOrder = item.topicOrder;
            topicType = item.topicType;
            if (topicType == 0){
              topicList[i].year = topicOrder.substr(0, 4);
              topicList[i].month = +(topicOrder.substr(5, 2));
              topicList[i].day = topicOrder.substr(8, 2);
            }
          })
          var arr = [];
          if (topicType == 0) {
            var group = that.groupBy(topicList);
            for(var i in group){
              arr.push({year:i,data:group[i]})
            }
          }else{
            arr = topicList;
          }
          that.setData({
            userStatus: userStatus,
            topicType: topicType,
            topicList: arr,
            show:true
          })
        }else{
          that.setData({
            show: true,
            userStatus: userStatus
          })
        }


      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  }
})