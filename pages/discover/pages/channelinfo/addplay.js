// pages/discover/pages/channelinfo/addplay.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    topicType:0,
    channelId: ''
  },
  changetype:function(e){
    var topicType = e.currentTarget.dataset.topictype;
    this.setData({
      topicType: topicType
    })
  },
  save:function(){
    var topicType = this.data.topicType;
    var channelId = this.data.channelId;
    wx.navigateTo({
      url: 'saveplay?topicType=' + topicType + '&channelId=' + channelId, 
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options);
    this.setData({
      channelId: options.channelId
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  }
})