import util from 'utils/util'
import common from 'utils/common'


App({
  onLaunch: function (ops) {
    common.login(ops);
    this.globalData.audioPlayer = wx.getBackgroundAudioManager();
  },
  onShow: function (ops) {
    console.log('app onshow');
    console.log(ops);
  },
  globalData:{
    audioPlayer: null
  }
})