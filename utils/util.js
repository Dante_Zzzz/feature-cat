//test
const server = 'https://www.talentboom.cn/backend/';
const domain = 'https://media.talentboom.cn/';
const appid = 'wx363876963aabd0d6';
const region = 'SCN';
const uploadURL = 'https://up-z2.qbox.me';

//prd
//上线需要改动app.js debug=false project.config.json appid
// const server = 'https://www.giftboom.cn/backend/';
// const domain = 'https://media.giftboom.cn/';
// const appid = 'wx384090b3770eb8d8';
// const region = 'SCN';
// ////// const uploadURL = 'https://upload-z2.qiniup.com';
// const uploadURL = 'https://up-z2.qbox.me';



const KTB_WXUSERNICKNAME = 'KTB_WXUSERNICKNAME';
const KTB_WXUSERAVATARURL =  'KTB_WXUSERAVATARURL';



const qiniuUploader = require("../pages/common/libs/qiniuUploader");

const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

// module.exports = {
//   formatTime: formatTime
// }

let util = {
  server : server,
  throttle(fn, gapTime) {
    if (gapTime == null || gapTime == undefined) {
      gapTime = 1500
    }

    let _lastTime = null

    // 返回新的函数
    return function () {
      let _nowTime = + new Date()
      if (_nowTime - _lastTime > gapTime || !_lastTime) {
        fn.apply(this, arguments)   //将this和参数传给原函数
        _lastTime = _nowTime
      }
    }
  },
  formarAudioTime(time){
    if(!time){
      return 0;
    }
    if(time<=60){
      return Math.ceil(time) +'"';
    }
    var min = Math.floor(time/60);
    var sec = Math.ceil(time % 60);
    return min + "'" + sec + '"';
  },
  
  getVideoSize(videoWidth, videoHeight){
    if (videoWidth > videoHeight) {
      videoHeight = videoHeight / 3;
      videoWidth = videoWidth / 3;
    } else {
      videoHeight = videoHeight / 3.5;
      videoWidth = videoWidth / 3.5;
    }
    if (videoWidth > 240) {
      videoHeight = videoHeight / (videoWidth / 240);
      videoWidth = 240;
    } else if (videoWidth < 120) {
      videoHeight = videoHeight / (videoWidth / 154);
      videoWidth = 154;
    }
    return { videoWidth: videoWidth, videoHeight: videoHeight}
  },
  
  /**
   * 获取deferred
   */
  getDeferred() {
    const deferred = {};

    deferred.promise = new Promise(function (resolve, reject) {
      deferred.resolve = resolve;
      deferred.reject = reject;
    });

    return deferred;
  },

  fail(){
    wx.hideLoading();
    wx.showToast({
      title: '网络不给力',
      icon: 'none',
      duration: 2000
    })
  },
  showToast(title){
    if(!title){
      return;
    }
    wx.showToast({
      title: title,
      icon:'none',
      duration: 2000
    })
  },
  upload(src,success,fail){
    if(!src){
      fail && fail();
      return;
    }
    qiniuUploader.upload(src, (res) => {
      success && success(res);
    }, (error) => {
      fail && fail();
      console.log('error: ' + error);
    }, {
        region: region,
        uploadURL: uploadURL,
        domain: domain,
        uptokenURL: server + 'getUploadToken?bucket=md-doc'
      }, (res) => {

      });
  },
  setUserInfoHandler(userInfo,success) {
    if(!userInfo){
      return;
    }
    util.wxRequest({
      url: 'updateUserInfo',
      data: { userInfo: userInfo },
      success: function (res) {
        success && success();
      },
      onfail:function(){

      },
      isHideLoading:true
    });
  },
  /**
   * url 请求地址
   * success 成功的回调
   * fail 失败的回调
   */
  wxRequest({url, data, success,onfail,isHideLoading,method = "POST"}) {
    if(!isHideLoading){
      wx.showLoading({mask:true});
    }
    let token = wx.getStorageSync('token'),
      that = this;
    if (token != "" && token != null) {
      var header = { 'content-type':'application/json','token': token };
    // } else {
    //   var header = { 'content-type': 'application/x-www-form-urlencoded' };
    }
    console.log(server + url + '--header:');
    console.log(header);
    console.log(server + url + '--request:');
    console.log(data);
    wx.request({
      url: server + url,
      method: method,
      data: data,
      header: header,
      success: (res) => {
        wx.hideLoading();
        console.log(server + url + '--response:');
        console.log(res);
        if(res['statusCode'] === 200){
          let data = res.data;
          if (data && data.code == 'CMN000000'){
            success && success(data);
          }else{
            if(onfail){
              onfail(data);
            }else{
              if (data && data.code){
                util.showToast(data.message);
              }else{
                that.fail();
              }
            }
          }
          //BUSINESS_ERROR = "CMN000005"
          //SYSTEM_ERROR = "CMN999999";
          //PARAM_VALIDATION_FAILED = "CMN000004";
        } else{
          that.fail();
        }
      },
      fail: function (res) {
        that.fail();
      },
      complete: function () {
        wx.hideLoading();
      }
    });
  }

 

}




export default util
