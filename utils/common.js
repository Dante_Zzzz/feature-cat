import util from '/util'
const KTB_WXUSERNICKNAME = 'KTB_WXUSERNICKNAME';
const KTB_WXUSERAVATARURL = 'KTB_WXUSERAVATARURL';

var app = getApp();
let common = {
  init() {
    var that = this;
    util.wxRequest({
      url: 'init',
      success: function (res) {
        var isGreyTest = res.isGreyTest;//是否灰度
        var userRole = res.userRole;//99管理员，1灰度用户,0普通用户
        var newMsgCount = res.newMsgCount;//消息数量
        //灰度期间，非管理员和灰度用户进内测中页面
        wx.setStorageSync('userRole', userRole);
        if (isGreyTest && (userRole != 99 && userRole != 1) ){
          wx.redirectTo({
            url: '/pages/index/index',
          })  
          return;
        }
        if (newMsgCount > 0){
          wx.showTabBarRedDot({
            index:2
          })
        }
        if (res.showPage == 0) {
          wx.switchTab({
            url: '/pages/tabBar/discover/index'
          })
          return;
        }
        //在判断当前页面是哪个，不存在页面属于异常情况，不是首页就返回
        var pages = getCurrentPages();
        if (pages && pages.length){
          var homepage = pages[0];
          // if (homepage.route != 'pages/tabBar/home/index'){
            
          // }
          homepage.init();
        }else{
          wx.switchTab({
            url: '/pages/tabBar/home/index'
          })
        }
 
          
        

      },
      onfail: function () {

      },
      isHideLoading: true
    })
  },


  login(ops) {
    wx.removeStorageSync('userRole');
    wx.removeStorageSync('token');
    wx.removeStorageSync('comment');
    wx.login({
      success: resp => {
        util.wxRequest({
          url: 'getTokenByCode',
          data: { 
            code: resp.code, 
            userInfo: { 
              gender:0,
              nickName:KTB_WXUSERNICKNAME, 
              avatarUrl:KTB_WXUSERAVATARURL
            } 
          },
          success: function (res) {
            wx.setStorageSync('token', res.token);
            common.init();

            if (ops.scene == 1044) {
              if (ops.shareTicket) {
                wx.getShareInfo({
                  shareTicket: ops.shareTicket,
                  success(res) {
                    console.log(res);
                    var encryptedData = res.encryptedData;
                    var iv = res.iv;
                    util.wxRequest({
                      url: 'updateGroupInfo',
                      data: { encryptedData: encryptedData, iv: iv },
                      success: function (res) {
                        console.log(res);
                      }
                    })
                  }
                })
              }


            }


          },
          onfail: function () {

          },
          isHideLoading: true
        });


        // wx.getUserInfo({
        //   withCredentials: true,
        //   success: res => {
        //     console.log("wx.getUserInfo:" + JSON.stringify(res));
        //     wx.setStorageSync('userInfo', res.userInfo);
        //     util.wxRequest({
        //       url: 'getTokenByCode',
        //       data: { 'code': resp.code, 'userInfo': res.userInfo },
        //       success: function (res) {
        //         wx.setStorageSync('token', res.token);
        //         common.init();
        //       }
        //     });
        //   },
        //   fail: () => {
        //     wx.showModal({
        //       title: '警告',
        //       content: '您点击了拒绝授权，将无法正常使用do+亲子的功能体验。请10分钟后再次点击授权，或者删除小程序重新进入。',
        //       success: function (res) {
        //         if (res.confirm) {
        //         }
        //       }
        //     })
        //   }
        // })


      }
    })
  },

  

};

export default common
